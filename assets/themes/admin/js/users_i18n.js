$(document).ready(function() {

    /**
     * Delete a user
     */
    $('.btn-delete-user').click(function() {
       window.location.href = $(this).attr('href') + "delete/" + $(this).attr('data-id');
    });

});

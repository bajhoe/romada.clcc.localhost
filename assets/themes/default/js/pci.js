/**
 * Default theme functions
 */
$(document).ready(function() {

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}


//=========================Table=========================
$('#table-pci').DataTable(

{
      dom: 'Bfrtip',
        buttons: [
            
            {extend: 'print',
            title: function(){
         return "C. PCI Value - "+nm_paket
      },
             exportOptions: {
                columns: [ 0, 1],
                stripHtml: false
            }}
        ],
    "responsive": true,
    "pageLength": 5,
  "search": {
    "smart": false
  },
    "lengthMenu": [ [5, 25, 50, -1], [5, 25, 50, "All"] ],
    "language" : {
            "lengthMenu": "Show _MENU_ row every page",
            "zeroRecords": "Data not found",
            "info": "Show page _PAGE_ of _PAGES_",
            "infoEmpty": "Data unavailable",
            "search":"Find",
            "infoFiltered": "(filtered from _MAX_ total records)"
            },
            "bFilter": false,
            "searching": false,
    "pagingType": "simple_numbers"
  
}
    );
//=========================Chart

var ctx = $("#thechart");
var databar1 = [];
var databar2 = [];
var databar3 = [];
for (var i = 0; i < charting.length; i++) {

nilai1 = "Segmen " + charting[i].tbl_segments;
nilai2 = charting[i].nilai_pci;
nilai3 = getRandomColor();
databar1.push(nilai1);
databar2.push(nilai2);
databar3.push(nilai3);
}

//alert (databar1);
var barData = {
  labels: databar1,
  datasets: [
    { 
fill: true,
      backgroundColor: 'rgba(0, 150, 100, 0.9)',
      label:"PCI Value",
      data: databar2,
      
  }]
};

var thechart = new Chart(ctx,{
    type: 'line',
    data: barData,
    options: {
         legend: {
            display: true
         }
    }
});
});

/*function resetFilter(){

    $("#segmens").val('');
    $("#segmens").keyup();
}*/


$(document).ready(function() {

    /**
     * Delete a ruas
     */
    $('.btn-delete-ruas').click(function() {
       window.location.href = $(this).attr('href') + "delete/" + $(this).attr('data-id');
    });

    $('#table-ruas tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text"/>' );
    } );

var table = $('#table-ruas').DataTable(

{
    dom: 'Bfrtip',
        buttons: [
            
            {extend: 'print',
             title: function(){
         return "Rekapitulasi Ruas Jalan"
      },
             exportOptions: {
                columns: [ 0, 1, 2,3,4,5,6],
                stripHtml: false
            }}
        ],
    responsive: true,
    "pageLength": 10,
    "lengthMenu": [ [5, 25, 50, -1], [5, 25, 50, "All"] ],
    language : {
            "lengthMenu": "Show _MENU_ row every page",
            "zeroRecords": "Data not found",
            "info": "Show page _PAGE_ of _PAGES_",
            "infoEmpty": "Data unavailable",
            "search":"Find",
            "infoFiltered": "(filtered from _MAX_ total records)"
            },
    
    columnDefs: [
    { "orderable": false,"targets": [0] }
                ],
                "bFilter": false
  
}
    );
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

});



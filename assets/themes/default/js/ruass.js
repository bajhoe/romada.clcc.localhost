$(document).ready(function() {


var options_propinsi = {
url: function(phrase) {
    return "/ruass/call_autocomplete";
  },

  ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
      dataType: "json"
    }
  },

  getValue: function(element) {
    return element.propinsi;
  },

  preparePostData: function(data) {
    data.phrase = $("#propinsi").val();
    data.jenis = "propinsi";
    return data;
  },
  requestDelay: 300
};


$("#propinsi").easyAutocomplete(options_propinsi);

var options_kecamatan = {
url: function(phrase) {
    return "/ruass/call_autocomplete";
  },

  ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
      dataType: "json"
    }
  },

  getValue: function(element) {
    return element.kecamatan;
  },

  preparePostData: function(data) {
    data.phrase = $("#kecamatan").val();
    data.jenis = "kecamatan";
    return data;
  },
  requestDelay: 300
};


$("#kecamatan").easyAutocomplete(options_kecamatan);


var options_kota_kab = {
url: function(phrase) {
    return "/ruass/call_autocomplete";
  },

  ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
      dataType: "json"
    }
  },

  getValue: function(element) {
    return element.kota_kab;
  },

  preparePostData: function(data) {
    data.phrase = $("#kota_kab").val();
    data.jenis = "kota_kab";
    return data;
  },
  requestDelay: 300
};


$("#kota_kab").easyAutocomplete(options_kota_kab);

var options_desa_kel = {
url: function(phrase) {
    return "/ruass/call_autocomplete";
  },

  ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
      dataType: "json"
    }
  },

  getValue: function(element) {
    return element.desa_kel;
  },

  preparePostData: function(data) {
    data.phrase = $("#desa_kel").val();
    data.jenis = "desa_kel";
    return data;
  },
  requestDelay: 300
};


$("#desa_kel").easyAutocomplete(options_desa_kel);

});



/**
 * Default theme functions
 */
$(document).ready(function() {


// load a locale
numeral.register('locale', 'id', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal : function (number) {
        return number === 1 ? 'er' : 'ème';
    },
    currency: {
        symbol: 'Rp. '
    }
});
// switch between locales
numeral.locale('id');

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

//=========================Chart1

var ctx = $("#chart1");
var databar1 = [];
var databar2 = [];
var databar3 = [];
var databar4 = [];
for (var i = 0; i < charting1.length; i++) {

nilai1 = "Segment " + charting1[i].segmen;
nilai2 = charting1[i].biaya;
nilai3 = getRandomColor();
nilai4 = charting1[i].niai_pci;
databar1.push(nilai1);
databar2.push(nilai2);
databar3.push(nilai3);
databar4.push(nilai4);
}

//alert (databar1);
var barData = {
  labels: databar1,
  datasets: [
    { 
fill: true,
      backgroundColor: 'rgba(0, 0, 150, 0.6)',
      label:"Cost Estimation",
      data: databar2,
      
  }]
};

var thechart = new Chart(ctx,{
    type: 'line',
    data: barData,
    options: {
         legend: {
            display: true
         },
         tooltips:{
          callbacks: {
          label: function(tooltipItem, data) {
          var string = numeral(parseFloat(tooltipItem.yLabel)).format("$0,0.00");
                return string;
            }
          }
         },
        scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value, index, values) {
          var string = numeral(parseFloat(value)).format("$0,0");
             return string;
            }
          }
        }]
    }
  }
});
//=========================Table1=========================
var table1 = $('#table-rekapitulasi-1').DataTable(
{
      dom: 'Bfrtip',
        buttons: [
            
            {extend: 'print',
            title: function(){
         return "F. Cost Est. By Segment - "+nm_paket
      },
             exportOptions: {
                columns: [ 0, 1, 2,3],
                stripHtml: false
            }}
        ],
    "responsive": true,
    "pageLength": 5,
    "search": {"smart": false},
    "lengthMenu": [ [5, 25, 50, -1], [5, 25, 50, "All"] ],
    "language" : {
      "decimal": ",",
      "thousands": ".",
      "lengthMenu": "Show _MENU_ rows every page",
      "zeroRecords": "Data can not be found",
      "info": "Page _PAGE_ of _PAGES_",
      "infoEmpty": "Data unavaliable",
      "search":"Find",
      "infoFiltered": "(filtered from _MAX_ total records)"},
    "bFilter": false,
    "searching": true,
    "pagingType": "simple_numbers",
    "columnDefs": [{
      "searchable": false,
      "orderable": false,
      "targets": 0}],
    "order": [[ 1, 'asc' ]]
});

    table1.on( 'order.dt search.dt', function () {
        table1.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();


//=========================Chart2

var datapie1 = [];
var datapie2 = [];
var datapie3 = [];
for (var i = 0; i < charting2.length; i++) {

nilai1 = charting2[i].distress;
nilai2 = charting2[i].biaya;
nilai3 = getRandomColor();
datapie1.push(nilai1);
datapie2.push(nilai2);
datapie3.push(nilai3);
}
var ctx = $("#chart2");
//alert (datapie2);
var pieData = {
  labels: datapie1,
  datasets: [
    { 
      data: datapie2,
      backgroundColor: datapie3
  }]
};

var myChart = new Chart(ctx,{
    type: 'pie',
    data: pieData,
    options: {
         legend: {
            display: true,
            position: 'top',
            labels: {
              fontSize: 10,
              boxWidth: 10
            }

         },
     tooltips:{
      callbacks: {
      label: function(tooltipItem, data) {
        var indice = tooltipItem.index;  
            return  data.labels[indice] +': '+ numeral(parseFloat(data.datasets[0].data[indice])).format('$0,0.00');
        }
      }
     },
    }
});
myChart.canvas.parentNode.style.height = '300px';

//=========================Table2=========================
var table2 = $('#table-rekapitulasi-2').DataTable(
{
      dom: 'Bfrtip',
        buttons: [
            
            {extend: 'print',
            title: function(){
         return "G. Cost Est. By Distress - "+nm_paket
      },
             exportOptions: {
                columns: [ 0, 1, 2,3],
                stripHtml: false
            }}
        ],
    "responsive": true,
    "pageLength": 5,
    "search": {"smart": false},
    "lengthMenu": [ [5, 25, 50, -1], [5, 25, 50, "All"] ],
    "language" : {
      "decimal": ",",
      "thousands": ".",
      "lengthMenu": "Show _MENU_ rows every page",
      "zeroRecords": "Data can not be found",
      "info": "Page _PAGE_ of _PAGES_",
      "infoEmpty": "Data unavaliable",
      "search":"Find",
      "infoFiltered": "(filtered from _MAX_ total records)"},
    "bFilter": false,
    "searching": true,
    "pagingType": "simple_numbers",
    "columnDefs": [{
      "searchable": false,
      "orderable": false,
      "targets": 0}],
    "order": [[ 1, 'asc' ]]
});

    table2.on( 'order.dt search.dt', function () {
        table2.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

});

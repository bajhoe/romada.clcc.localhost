/**
 * Default theme functions
 */
$(document).ready(function() {

//==============================MAPS

var streets = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiYmFqaG9lIiwiYSI6ImNqZjd1bXkyOTJ0cGIyd3BuYjE2b2txcDUifQ.zIm0k9YEj_6OFEb8O13osw'
});

var map = L.map('map',{
                          center : [-7.568652,110.827959], 
                          zoom : 16,
                          layers: [streets]
                        });
var marker;
var polyline1;

for (var i = 0; i < latlong.length; i++) {
    var j = i + 1;
            marker = new L.marker([latlong[i][0],latlong[i][1]]);
            marker.bindTooltip("STA "+ [j]);
  map.addLayer(marker);
            }


$('a[href="#stationing"]').on('shown.bs.tab', function (e) {


        });


var polyline = new L.polyline(latlong, {color: 'black', 
                                   weight: '3',
                               opacity: 0.5,
                                smoothFactor: 1
                            }
                                   );
map.addLayer(polyline);
map.fitBounds(polyline.getBounds());


//=========================Table=========================
/*$('#table-stationing').DataTable(

{
    dom: 'Bfrtip',
        buttons: [
            
            {extend: 'print',
             title: function(){
         return "A. Stationing - "+nm_paket
      },
             exportOptions: {
                columns: [ 0, 1, 2],
                stripHtml: false
            }}
        ],
    responsive: true,
    "pageLength": 5,
    "lengthMenu": [ [5, 25, 50, -1], [5, 25, 50, "All"] ],
    language : {
            "lengthMenu": "Show _MENU_ row every page",
            "zeroRecords": "Data not found",
            "info": "Show page _PAGE_ of _PAGES_",
            "infoEmpty": "Data unavailable",
            "search":"Find",
            "infoFiltered": "(filtered from _MAX_ total records)"
            },
    
    columnDefs: [
    { "orderable": false,"targets": [0] }
                ],
                "bFilter": false
  
}
    );
*/






});


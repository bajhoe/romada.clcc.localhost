/**
 * Default theme functions
 */
$(document).ready(function() {
 $('#table-isi').DataTable(

{
        dom: 'Bfrtip',
        buttons: [
            
            {extend: 'print',
             exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                stripHtml: false
            }}
        ],
	responsive: true,

	language : {
            "lengthMenu": "Show _MENU_ row every page",
            "zeroRecords": "Data not found",
            "info": "Show page _PAGE_ of _PAGES_",
            "infoEmpty": "Data unavailable",
            "search":"Find",
            "infoFiltered": "(filtered from _MAX_ total records)"
            },
	
	columnDefs: [
    { "orderable": false,"targets": [0,10] }
  				]
  
}

 	);
});

/**
 * Default theme functions
 */
$(document).ready(function() {

$('#table-isi2 tr').bind('click', function(e) {
    $('#table-isi2 tr').css('background-color','');
    $(e.currentTarget).css('background-color','rgba(204, 255, 153, 0.6)');
})

$('#modal-delete').on('show.bs.modal', function(e) {
    var distress = $(e.relatedTarget).data('distress');
    var ahspkId = $(e.relatedTarget).data('ahspk-id');
    var mBayar  = $(e.relatedTarget).data('distress1');
    var nama  = $(e.relatedTarget).data('distress2');
    $(e.currentTarget).find('p[id="tanya-delete"]').html('Are you sure want to delete <b>'+ mBayar +' | '+ nama + ' </b>in distress type: <b>' + distress + ' </b>?');
    $('#button-delete-hsp').attr('data-id-delete' , ahspkId);
});

$('#modal-add').on('show.bs.modal', function(e) {
    
    $(".item_ahspk").val(null).trigger('change');
    var row_id = $(e.relatedTarget).data('row-id');
    var distress_id = $(e.relatedTarget).data('distress-id');
    var distress_name = $(e.relatedTarget).data('distress-name');
    $(e.currentTarget).find('p[id="tanya-add"]').html('Please choose work item for distress type: <b>'+ distress_name +' </b>:');
    $('#button-add-hsp').attr('data-row-add' , row_id);
    $('#button-add-hsp').attr('data-id-add' , distress_id);
    $('#button-add-hsp').attr('disabled' , 'disabled');
    $(".item_ahspk option").removeAttr("disabled");
    $(".item_ahspk").select2(
        {  
          dropdownParent: $("#modal-add"),
          placeholder: 'Choose Work Item',
          allowClear: true,
          theme : "classic"
        });
    $.ajax
      ({
        url: "distress_ahspks/get_hsps",
        type: "POST",
        dataType:'JSON', 
        data: {id:distress_id},
        success : function(response)
          {
           
                if (response.ahspks !== null) {
                for (var i = response.ahspks.length - 1; i >= 0; i--) {
                    $(".item_ahspk option[value="+response.ahspks[i].id_ref_seksi_ahspk+"]").attr("disabled","disabled");
                }}
             else {
          }
      }
      });
});



 $('.item_ahspk').on('change', function(e) {
      var data = $(this).val();
       if(data !== ''){
        $('#button-add-hsp').attr('data-hsp-add' , data);
        $('#button-add-hsp').removeAttr('disabled' , 'disabled');
        } else {
        $('#button-add-hsp').attr('data-hsp-add' , '');
        $('#button-add-hsp').attr('disabled' , 'disabled');
        }
    });


/**
 * Delete hsp
 */
$('.btn-delete-hsp').click(function() {
   //window.location.href = $(this).attr('href') + "delete/" + $(this).attr('data-id-delete');
   //alert($(this).attr('href') + "delete/" + $(this).attr('data-id-delete'));
  
      //$('.loading').show();  // show the loading message.
      $.ajax
      ({
        url: $(this).attr('href')+"delete",
        type: "POST",
        dataType:'JSON', 
        data: {id:$(this).attr('data-id-delete')},
        success : function(response)
          {
            if (response.status === "TRUE") {
                location.reload();
            } else {
             alert ('Gagal');
          }
      }
      });
});

/**
 * Add hsp
 */
$('.btn-add-hsp').click(function() {
   //window.location.href = $(this).attr('href') + "delete/" + $(this).attr('data-id-delete');
   //alert($(this).attr('href') + "delete/" + $(this).attr('data-id-delete'));
  var row = $(this).attr('data-row-add');
  var col = 3;
  var id = $(this).attr('data-id-add');
  var hsp = $(this).attr('data-hsp-add');
  var tahun = $(this).attr('data-tahun-add')
      //$('.loading').show();  // show the loading message.
      $.ajax
      ({
        url: $(this).attr('href')+"add",
        type: "POST",
        dataType:'JSON', 
        data: {id_ref_pci_distress:id, id_ref_seksi_ahspk:hsp, tahun_ahspk:tahun},
        success : function(response)
          {
            if (response.status === "TRUE") {
                location.reload();
                //alert(col + ' dan ' + row)
                //$(this).attr('data-row-add')

            } else {
             alert ('Gagal');
          }
      }
      });
});

 $('#table-isi2').DataTable(

{
        dom: 'Bfrtip',
        buttons: [
            
            {extend: 'print',
             exportOptions: {
                columns: [ 0, 1, 2, 3, 4],
                stripHtml: false
            }}
        ],
    responsive: true,
    stateSave: true,
    rowId: 'No',
    select: true,
    language : {
            "lengthMenu": "Show _MENU_ row every page",
            "zeroRecords": "Data not found",
            "info": "Show page _PAGE_ of _PAGES_",
            "infoEmpty": "Data unavailable",
            "search":"Find",
            "infoFiltered": "(filtered from _MAX_ total records)"
            },
    
    columnDefs: [
    { "orderable": false,"targets": [0,4] }
                ]
  
}

    );
});


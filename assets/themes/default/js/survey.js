/**
 * Default theme functions
 */
$(document).ready(function() {

//==============================MAPS
//Set Map Tile Layer
var streets = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiYmFqaG9lIiwiYSI6ImNqZjd1bXkyOTJ0cGIyd3BuYjE2b2txcDUifQ.zIm0k9YEj_6OFEb8O13osw'
});
//Set Map
var map = L.map('map',{
                          center : [-7.568652,110.827959], 
                          zoom : 16,
                          layers: [streets]
                        });

//polyline path anim
$('a[href="#stationing"]').on('shown.bs.tab', function (e) {

        });

var polyline1 = L.polyline.antPath(latlong2, {color: 'yellow', 
                                   weight: '12',
                                   delay: '800',
                               opacity: 0.4,
                                smoothFactor: 1
                            }
                                   );
polyline1.addTo(map);
map.fitBounds(polyline1.getBounds());

//polyline for segment
for (var i = 0; i < latlong1.length; i++) {
var pointA = [latlong1[i][0],latlong1[i][1]];
var pointB = [latlong1[i][2],latlong1[i][3]];
var pointList = [pointA, pointB];
var polyline = new L.polyline(pointList, {color: getRandomColor(), 
                                   weight: '12',
                                  opacity: 0.4,
                                 
                                 });
polyline.bindPopup("Segment " + [i+1] + "<br />Length " + latlong1[i][4] + " m");
polyline.addTo(map);
//event for mouse click on segment
polyline.on('click', L.bind(onMapClick, null, latlong1[i][4], i+1));
}



//Function trigerred by mouse click event
function onMapClick(param0, param1) {
      $("#segmenspan").html(param1);
      $("#jarak").html(param0 + " m");
      applyFilter(param1);
 }


function applyFilter(filter) {

    var filterCtrl= $("#segmens");
    filterCtrl.val('');
    filterCtrl.val(filter);
    filterCtrl.keyup();
}






function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}


//=========================Table=========================
$('#table-survey').DataTable(

{
	        dom: 'Bfrtip',
        buttons: [
            
            {extend: 'print',
             title: function(){
         return "2. Survei Kerusakan - Ruas Jalan "+nm_paket
      },
      autoPrint: false,
      customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
 
                    $(win.document.body).find( 'h1' )
                        .css( 'font-size', '14pt' );

                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
             exportOptions: {
             	columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ],
                stripHtml: false
            }}
        ],
    "responsive": true,
    "pageLength": 5,
  "search": {
    "smart": false
  },
    "lengthMenu": [ [5, 25, 50, -1], [5, 25, 50, "All"] ],
    "language" : {
            "lengthMenu": "Show _MENU_ row every page",
            "zeroRecords": "Data not found",
            "info": "Show page _PAGE_ of _PAGES_",
            "infoEmpty": "Data unavailable",
            "search":"Find",
            "infoFiltered": "(filtered from _MAX_ total records)"
            },
    
    "columnDefs": [
    { "orderable": false,"targets": [8] }
                ],
                "bFilter": false,
                "searching": true,
  
}
    );

 $('#table-survey tfoot th:first').html( '<input style="display:none" id="segmens" type="text" placeholder="" />' );
 
 
    // DataTable
    var table = $('#table-survey').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {

              regExSearch = this.value;

                that
                    .search( regExSearch ? '^'+regExSearch+'$' : '', true, false )
                    .draw();
            }
        } );
    } );

//=========================Chart

var datapie1 = [];
var datapie2 = [];
var datapie3 = [];
for (var i = 0; i < charting.length; i++) {

nilai1 = charting[i].jml;
nilai2 = charting[i].distress_name;
nilai3 = getRandomColor();
datapie1.push(nilai1);
datapie2.push(nilai2);
datapie3.push(nilai3);
}
var ctx = $("#chart");
//alert (datapie2);
var pieData = {
  labels: datapie2,
  datasets: [
    { 
      data: datapie1,
      backgroundColor: datapie3
  }]
};

var myChart = new Chart(ctx,{
    type: 'pie',
    data: pieData,
    options: {
         legend: {
            display: false
         }
    }
});




});

function resetFilter(){

    $("#segmens").val('');
    $("#segmens").keyup();
}
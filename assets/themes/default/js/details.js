/**
 * Default theme functions
 */
$(document).ready(function() {

//==============================MAPS

/*var streets = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiYmFqaG9lIiwiYSI6ImNqZjd1bXkyOTJ0cGIyd3BuYjE2b2txcDUifQ.zIm0k9YEj_6OFEb8O13osw'
});


var map0 = L.map('map0',{
                          center : [-7.568652,110.827959], 
                          zoom : 16,
                          layers: [streets]


                        });*/
/*var marker;
var polyline1;

for (var i = 0; i < latlong.length; i++) {
    var j = i + 1;
            marker = new L.marker([latlong[i][0],latlong[i][1]]);
marker.bindTooltip("STA "+ [j]);
  
            }

var emge = L.layerGroup([marker])

$('a[href="#stationing"]').on('shown.bs.tab', function (e) {


//map0.fitBounds(featuregroup.getBounds());

        });

$('a[href="#pci"]').on('shown.bs.tab', function (e) {

if (map.hasLayer(polyline)){

map.removeLayer(polyline.antPath);
}
 
}); */



/*$('a[href="#survey"]').on('shown.bs.tab', function (e) {

var polyline = L.polyline.antPath(latlong, {color: 'black', 
                                   delay: '800',
                                   weight: '9'}
                                   );
map.addLayer(polyline);
map.fitBounds(polyline.getBounds());

});*/


var layers = [];
var i = 0;
var map = "map";
for(var i=0;i<1;i++){
  layers[i] = L.map(map.concat(i));
  var ref = layers[i];
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiYmFqaG9lIiwiYSI6ImNqZjd1bXkyOTJ0cGIyd3BuYjE2b2txcDUifQ.zIm0k9YEj_6OFEb8O13osw'
}).addTo(ref);
}
//=========================Table=========================
$('#table-stationing').DataTable(

{
    responsive: true,
    "pageLength": 5,
    "lengthMenu": [ [5, 25, 50, -1], [5, 25, 50, "All"] ],
    language : {
            "lengthMenu": "Show _MENU_ row every page",
            "zeroRecords": "Data not found",
            "info": "Show page _PAGE_ of _PAGES_",
            "infoEmpty": "Data unavailable",
            "search":"Find",
            "infoFiltered": "(filtered from _MAX_ total records)"
            },
    
    columnDefs: [
    { "orderable": false,"targets": [0,3] }
                ],
                "bFilter": false
  
}
    );







});


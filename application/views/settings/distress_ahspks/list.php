<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6 text-left">
                <h3 class="panel-title"><?php //echo lang('distress_ahspks title paket_list'); ?></h3>
            </div>
        </div>
    </div>
<div class="table-responsive" style="padding:5px">
    <table class="table table-striped table-hover-warning" id="table-isi2" style="width:100%">
        <thead>
            <tr>
                <td>
                   <b> <?php echo lang('distress_ahspks col no'); ?> </b>
                </td>
                <td>
                   <b> <?php echo lang('distress_ahspks col distress_name'); ?> </b>
                </td>
                <td style="width:200px">
                   <b> <?php echo lang('distress_ahspks col definition'); ?> </b>
                </td>
                <td style="width:100px">
                   <b> <?php echo lang('distress_ahspks col treatment'); ?> </b>
                </td>
                <td>
                   <b> <?php echo lang('distress_ahspks col ahspk'); ?> </b>
                </td>
            </tr>
        </thead>
        <tbody>

            <?php // data rows ?>
            <?php 
                $no = '';
                //(($offset) ? $no = $offset + 1 : $no = 1);
                $no = 1;
            ?>
            <?php if ($total) : ?>
                <?php foreach ($distresses as $distress) : ?>
                    <tr>
                        <td>
                            <?php echo $no; ?>
                        </td>
                        <td>
                            <?php echo $distress['distress_name']." (".$distress['distress_level'].") (".$distress['distress_type'].") (".$distress['distress_unit'].")"; ?>
                        </td>
                        <td>
                            <?php echo $distress['distress_level_def']; ?>
                        </td>
                        <td>
                            <?php echo $distress['distress_level_treat']; ?>
                        </td>
                        <td>
                            <span>                            <a class="btn btn-warning tooltips btn-xs btn-add" href="#modal-add" title="<?php echo lang('distress_ahspks tooltip add_new_paket') ?>" data-toggle="modal" 
                                data-row-id="<?php echo $no; ?>"
                                data-distress-id="<?php echo $distress['id']; ?>"
                                data-distress-name="<?php echo $distress['distress_name']." (".$distress['distress_level'].") (".$distress['distress_type'].")"; ?>"
                                ><span class="glyphicon glyphicon-plus-sign"></span></a></span>
                            <ul>
                            <?php 
                            if ($distresses_ahspks != NULL) {
                            
                            foreach ($distresses_ahspks as $data) : ?>
                              <?php  if ($data['id_ref_pci_distress'] == $distress['id']) :
                                echo "<li>".
                                $data['mbayar_ahspk']." | ".$data['nama_ahspk']." | Rp. ".number_format($data['harsat_ahspk'],2,'.',',')." | ".$data['satuan_ahspk'];?>

                                <a href="#modal-delete" 
                                data-ahspk-id="<?php echo $data['id_distress_ahspk']; ?>"
                                data-distress="<?php echo $distress['distress_name']." (".$distress['distress_level'].") (".$distress['distress_type'].")"; ?>" 
                                data-distress1="<?php echo $data['mbayar_ahspk']; ?>" 
                                data-distress2="<?php echo $data['nama_ahspk']; ?>" data-toggle="modal" class="btn btn-danger btn-xs" title="<?php echo lang('admin button delete'); ?>">
                                <span class="glyphicon glyphicon-trash"></span></a>
                                    </li>
                                
                            <?php endif; ?>
                            <?php endforeach; }?>

                             
                             </ul>
                        </td>   
                    </tr>
                    <?php $no++; ?>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="6">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>

        </tbody>
        <tfoot>
        </tfoot>
    </table>
</div>
<?php //dtc($distresss); ?>
</div>

<?php // delete modal ?>
        <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-label-delete" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="modal-label-delete"><?php echo lang('distress_ahspks title paket_delete');  ?></h4>
                    </div>
                    <div class="modal-body">
                        <p id="tanya-delete"></p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-warning btn-cancel" type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                        <button id="button-delete-hsp" type="button" class="btn btn-primary btn-delete-hsp" href="<?php echo site_url('settings/distress_ahspks/'); ?>" data-id-delete=""><?php echo lang('admin button delete'); ?></button>
                    </div>
                </div>
            </div>
        </div>

<?php // add modal ?>
        <div class="modal fade modal-add" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-label-add" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="modal-label-delete"><?php echo lang('distress_ahspks title paket_add');  ?></h4>
                    </div>
                    <div class="modal-body">
                       <p id="tanya-add"></p>

<select name="item_ahspk" id="item_ahspk" class="item_ahspk" style="width:100%;">                  
    <option value=""></option>            
          <?php  if ($ahspks) : ?>
            
            <?php //echo var_dump($ahspks); ?>
            <?php 
    $groups = array();
    foreach ($ahspks as $pilihan) {
        $groups[$pilihan['nama_divisi']][$pilihan['id_ahspk']] = $pilihan['mbayar_ahspk']." | ".$pilihan['nama_ahspk']." | ".$pilihan['satuan_ahspk']." | Rp. ".number_format($pilihan['harsat_ahspk'],2,",",".");
    }
    foreach($groups as $label => $opt): ?>
        <optgroup label="<?php echo $label; ?>">
    <?php foreach ($opt as $id => $name): ?>
        <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
    <?php endforeach; ?>
    </optgroup>
<?php endforeach; ?>
            
           <?php endif; ?>

</select>



                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-warning btn-cancel" type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                        <button id="button-add-hsp" type="button" class="btn btn-primary btn-add-hsp" href="<?php echo site_url('settings/distress_ahspks/'); ?>" data-tahun-add="<?php echo $tahun; ?>" data-row-add="" data-id-add="" data-hsp-add=""><?php echo lang('admin button add'); ?></button>
                    </div>
                </div>
            </div>
        </div>

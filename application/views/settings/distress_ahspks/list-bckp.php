<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6 text-left">
                <h3 class="panel-title"><?php //echo lang('distress_ahspks title paket_list'); ?></h3>
            </div>
            <div class="col-md-6 text-right">
                <a class="btn btn-default tooltips btn-sm" href="<?php echo base_url('admin/pakets/add'); ?>" title="<?php echo lang('distress_ahspks tooltip add_new_paket') ?>" data-toggle="tooltip"><span class="glyphicon glyphicon-plus-sign"></span> <?php echo lang('distress_ahspks button add_new_paket'); ?></a>
            </div>
        </div>
    </div>
<div class="table-responsive" style="padding:5px">
    <table class="table table-striped table-hover-warning" id="table-isi2" style="width:100%">
        <thead>
            <tr>
                <td>
                    <?php echo lang('distress_ahspks col no'); ?>
                </td>
                <td>
                    <?php echo lang('distress_ahspks col distress_name'); ?>
                </td>
                <td>
                    <?php echo lang('distress_ahspks col ahspk'); ?>
                </td>
                <td style="min-width:100px">
                    <?php echo lang('admin col actions'); ?>
                </td>
            </tr>
        </thead>
        <tbody>

            <?php // data rows ?>
            <?php 
                $no = '';
                //(($offset) ? $no = $offset + 1 : $no = 1);
                $no = 1;
            ?>
            <?php if ($total) : ?>
                <?php foreach ($distresses as $distress) : ?>
                    <tr>
                        <td>
                            <?php echo $no; ?>
                        </td>
                        <td>
                            <?php echo $distress['distress_name']." (".$distress['distress_level'].") (".$distress['distress_type'].")"; ?>
                        </td>
                        <td>
                            <ul>
                            <?php 
                            foreach ($distresses_ahspks as $data) : ?>
                              <?php  if ($data['id_ref_pci_distress'] == $distress['id']) :
                                    echo "<li>".$data['mbayar_ahspk']." | ".$data['nama_ahspk']." | Rp. ".number_format($data['harsat_ahspk'],2,',','.')." | ".$data['satuan_ahspk'];?>

                                    <a href="#modal-<?php echo $data['id_distress_ahspk']; ?>" data-toggle="modal" class="btn btn-danger btn-sm" title="<?php echo lang('admin button delete'); ?>">
<span class="glyphicon glyphicon-trash"></span></a>
                                    </li>
                                
                            <?php endif; ?>
                            <?php endforeach; ?>

                             
                             </ul>
                        </td>   
                        <td>
                        	
                            <div>
                                <div class="btn-group">
                             
<!--                                     <a href="#modal-<?php //echo $distress['id_paket']; ?>" data-toggle="modal" class="btn btn-danger btn-sm" title="<?php //echo lang('admin button delete'); ?>">
<span class="glyphicon glyphicon-trash"></span></a>
                             
<a href="<?php //echo $this_url; ?>/edit/<?php //echo $distress['id_paket']; ?>" class="btn btn-warning btn-sm" title="<?php //echo lang('admin button edit'); ?>"><span class="glyphicon glyphicon-pencil"></span></a> -->
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php $no++; ?>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="5">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>

        </tbody>
        <tfoot>
        </tfoot>
    </table>
</div>
<?php //dtc($distresss); ?>
</div>

<?php // delete modal ?>
<?php if ($total) : ?>
    <?php foreach ($distresses_ahspks as $data) : ?>
        <div class="modal fade" id="modal-<?php echo $data['id_distress_ahspk']; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-label-<?php echo $data['id_distress_ahspk']; ?>" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="modal-label-<?php echo $data['id_distress_ahspk']; ?>"><?php echo lang('distress_ahspks title paket_delete');  ?> pada Jenis Kerusakan ini?</h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo sprintf(lang('distress_ahspks msg delete_confirm'), $data['mbayar_ahspk'] . " | " . $data['nama_ahspk']); ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                        <button type="button" class="btn btn-primary btn-delete-paket" href="<?php echo site_url('admin/pakets/'); ?>" data-id="<?php echo $data['id_distress_ahspk']; ?>"><?php echo lang('admin button delete'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

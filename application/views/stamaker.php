<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-default">
    <div class="panel panel-heading">
       <a href="<?php echo site_url('pakets'); ?>" class="btn btn-info btn-sm" title="Return to Project List"><span class="fa fa-arrow-left"></span> Return to Project List</a>
    </div>
<div class="panel panel-body">
 <div class="row">
    <div class="col-md-12"> 
      <!-- Nav tabs -->
      <div class="card">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="<?php echo site_url('details/stationing/'); ?><?php echo $paket_id; ?>" aria-controls="stationing"><i class="fa fa-map-pin"></i>  <span>Station Maker</span></a>
          </li>
        </ul>

        <!-- Tab panes -->
        <?php //STATIONING ?>
        <div class="tab-content" style="padding:5px;">
          <div role="tabpanel" class="tab-pane active" id="stationing">
                <div class="col-md-6" style="padding:5px;"> 
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <h3 class="panel-title">Map : <?php echo $paket; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">       
                                <div id="map"></div>
                            </div>
                        </div>   
                </div>

                <div id="summary-stationing" class="col-md-6" style="padding:5px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <h3 class="panel-title">Information : <?php echo $paket; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="min-height:300px;"> 
                            <h4><?php echo $paket; ?></h4>      
                                    <p>Road Name : <?php echo $ruas['nm_ruas']; ?></p>
                                    <p>Road Length : <?php echo $ruas['panjang']." km"; ?></p>
                                    <p>Road Width : <?php echo $ruas['lebar']." m"; ?></p>
                                    <p>Total Stations: <?php echo count($stations); ?></p>
                                    <p>Total Segments: <?php echo count($segments); ?></p>
                                    <p>Function : <?php echo $ruas['fungsi']; ?></p>
                                    <p>Category : <?php echo $ruas['kategori']; ?></p>
                                    <p>Sector Supported : <?php echo $ruas['mendukung']; ?></p>

                            </div>
                        </div>
                </div>
                <div class="table-responsive col-md-12" style="padding:5px">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <h3 class="panel-title">Table : <?php echo $paket; ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body"> 
                <table class="table table-striped table-hover-warning" id="table-stationing" style="width:100%">
                    <thead>
                        <tr>
                            <td>
                                <?php echo lang('stations col no_station'); ?>
                            </td>
                            <td>
                                <?php echo lang('stations col lat'); ?>
                            </td>
                            <td>
                               <?php echo lang('stations col lon'); ?>
                            </td>
<!--                             <td style="min-width:100px">
    <?php //echo lang('admin col actions'); ?>
</td> -->
                        </tr>
                    </thead>
                    <tbody>

                        <?php // data rows ?>
                       
                            <?php foreach ($stations as $station) : ?>
                                <tr>
                                    <td>
                                        <?php echo $station['no_station']; ?>
                                    </td>
                                    <td>
                                        <?php echo $station['lat']; ?>
                                    </td>
                                    <td>
                                        <?php echo $station['lon']; ?>
                                    </td>
<!--                                   <td>
    
    <div>
        <div class="btn-group">
     
            <a href="#modal-<?php echo $station['id_station']; ?>" data-toggle="modal" class="btn btn-danger btn-sm" title="<?php echo lang('admin button delete'); ?>">
            <span class="glyphicon glyphicon-trash"></span></a>
     
            <a href="<?php echo $this_url; ?>/edit/<?php echo $station['id_station']; ?>" class="btn btn-warning btn-sm" title="<?php echo lang('admin button edit'); ?>"><span class="glyphicon glyphicon-pencil"></span></a>

        </div>
    </div>
</td>  -->
                                </tr>
                            <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
            </div>
        </div>
    </div>


        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script>
    var nm_paket = "<?php echo $paket; ?>";
    var latlong = <?php echo latlon($stations); ?>

</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6 text-left">
                <h3 class="panel-title"><?php echo lang('tahuns title tahun_list'); ?></h3>
            </div>
 <div class="col-md-6 text-right">
    <a class="btn btn-xs btn-success tooltips" href="<?php echo base_url('admin/tahuns/add'); ?>" title="<?php echo lang('tahuns tooltip add_new_tahun') ?>" data-toggle="tooltip"><span class="glyphicon glyphicon-plus-sign"></span> <?php echo lang('tahuns button add_new_tahun'); ?></a>
</div> 
        </div>
    </div>
    <div class="table-responsive">
    <table class="table table-striped table-hover-warning">
        <thead>

            <?php // sortable headers ?>
            <tr>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=tahuns_id&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('tahuns col tahuns_id'); ?></a>
                    <?php if ($sort == 'tahuns_id') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=tahuns_name&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('tahuns col tahun'); ?></a>
                    <?php if ($sort == 'tahuns_name') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=status&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('admin col status'); ?></a>
                    <?php if ($sort == 'status') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td class="pull-right">
                    <?php echo lang('admin col actions'); ?>
                </td>
            </tr>

            <?php // search filters ?>
<!--             <tr>
    <?php //echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
        <th>
        </th>
        <th<?php //echo ((isset($filters['tahun'])) ? ' class="has-success"' : ''); ?>>
            <?php //echo form_input(array('name'=>'tahun', 'id'=>'tahun', 'class'=>'form-control input-sm', 'placeholder'=>lang('tahuns input tahun'), 'value'=>set_value('tahun', ((isset($filters['tahuns_name'])) ? $filters['tahuns_name'] : '')))); ?>
        </th>
        <th colspan="3">
            <div class="text-right">
                <a href="<?php //echo $this_url; ?>" class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="glyphicon glyphicon-refresh"></span> <?php //echo lang('core button reset'); ?></a>
                <button type="submit" name="submit" value="<?php //echo lang('core button filter'); ?>" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="<?php //echo lang('admin tooltip filter'); ?>"><span class="glyphicon glyphicon-filter"></span> <?php //echo lang('core button filter'); ?></button>
            </div>
        </th>
    <?php echo form_close(); ?>
</tr> -->

        </thead>
        <tbody>

            <?php // data rows ?>
            <?php if ($total) : ?>
                <?php foreach ($tahuns as $tahun) : ?>
                    <tr>
                        <td<?php echo (($sort == 'id') ? ' class="sorted"' : ''); ?>>
                            <?php echo $tahun['tahuns_id']; ?>
                        </td>
                        <td<?php echo (($sort == 'tahun') ? ' class="sorted"' : ''); ?>>
                            <?php echo $tahun['tahuns_name']; ?>
                        </td>
                        <td<?php echo (($sort == 'status') ? ' class="sorted"' : ''); ?>>
                            <?php echo ($tahun['status']) ? '<span class="active">' . lang('admin input active') . '</span>' : '<span class="inactive">' . lang('admin input inactive') . '</span>'; ?>
                        </td>
                        <td>
                            <div class="text-right">
                                <div class="btn-group">
                                    <?php //if ($tahun['tahuns_id'] > 175) : ?>
                                        <a href="#modal-<?php echo $tahun['tahuns_id']; ?>" data-toggle="modal" class="btn btn-danger btn-xs" title="<?php echo lang('admin button delete'); ?>"><span class="glyphicon glyphicon-trash"></span></a>
                                    <?php //endif; ?>
                                    <a href="<?php echo $this_url; ?>/edit/<?php echo $tahun['tahuns_id']; ?>" class="btn btn-warning btn-xs" title="<?php echo lang('admin button edit'); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="7">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>

        </tbody>
    </table>
    </div>
    <?php // list tools ?>
    <div class="panel-footer">
        <div class="row">
            <div class="col-md-2 text-left">
                <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
            </div>
            <div class="col-md-2 text-left">
                <?php if ($total > 10) : ?>
                    <select id="limit" class="form-control">
                        <option value="10"<?php echo ($limit == 10 OR ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                        <option value="25"<?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                        <option value="50"<?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                        <option value="75"<?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                        <option value="100"<?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                    </select>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php echo $pagination; ?>
            </div>
            <div class="col-md-2 text-right">
<!--                 <?php if ($total) : ?>
    <a href="<?php //echo $this_url; ?>/export?sort=<?php //echo $sort; ?>&dir=<?php //echo $dir; ?><?php //echo $filter; ?>" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="<?php //echo lang('admin tooltip csv_export'); ?>"><span class="glyphicon glyphicon-export"></span> <?php //echo lang('admin button csv_export'); ?></a>
<?php endif; ?> -->
            </div>
        </div>
    </div>

</div>

<?php // delete modal ?>
<?php if ($total) : ?>
    <?php foreach ($tahuns as $tahun) : ?>
        <div class="modal fade" id="modal-<?php echo $tahun['tahuns_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-label-<?php echo $tahun['tahuns_id']; ?>" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="modal-label-<?php echo $tahun['tahuns_id']; ?>"><?php echo lang('tahuns title tahun_delete');  ?></h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo sprintf(lang('tahuns msg delete_confirm'), $tahun['tahuns_name'])?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                        <button type="button" class="btn btn-primary btn-delete-tahun"  href="<?php echo base_url(); ?>admin/tahuns/" data-id="<?php echo $tahun['tahuns_id']; ?>"><?php echo lang('admin button delete'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php echo form_open('', array('role'=>'form')); ?>

    <?php // hidden id ?>
    <?php if (isset($roles)) : ?>
        <?php echo form_hidden('roles_id', $roles['roles_id']);
         ?>
    <?php endif; ?>

<div class="row">
            <?php // role ?>
        <div class="form-group col-sm-4<?php echo form_error('role') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('roles input role'), 'role', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'roles_name', 'value'=>set_value('roles_name', (isset($roles['roles_name']) ? $roles['roles_name'] :'')), 'class'=>'form-control')); ?>
        </div>

        <?php // status ?>
        <div class="form-group col-sm-3<?php echo form_error('status') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('roles input status'), '', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <div class="radio">
                <label>
                    <?php echo form_radio(array('name'=>'status', 'roles_id'=>'radio-status-1', 'value'=>'1', 'checked'=>(( ! isset($roles['status']) OR (isset($roles['status']) && (int)$roles['status'] == 1) OR $roles['roles_id'] == 1) ? 'checked' : FALSE))); ?>
                    <?php echo lang('admin input active'); ?>
                </label>
            </div>
         
                <div class="radio">
                    <label>
                        <?php echo form_radio(array('name'=>'status', 'roles_id'=>'radio-status-2', 'value'=>'0', 'checked'=>((isset($roles['status']) && (int)$roles['status'] == 0) ? 'checked' : FALSE))); ?>
                        <?php echo lang('admin input inactive'); ?>
                    </label>
                </div>
         
        </div>

</div>



    <?php // buttons ?>
    <div class="row pull-right">
        <a class="btn btn-link" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
        <button type="submit" name="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
    </div>

<?php echo form_close(); ?>

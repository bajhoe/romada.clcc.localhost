<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6 text-left">
            </div>
            <div class="col-md-6 text-right">
                <a class="btn btn-success tooltips" href="<?php echo base_url('ruass/add'); ?>" title="<?php echo lang('ruass tooltip add_new_ruas') ?>" data-toggle="tooltip"><span class="glyphicon glyphicon-plus-sign"></span> <?php echo lang('ruass button add_new_ruas'); ?></a>
            </div>
        </div>
    </div>
<div class="table-responsive">
    <table id="table-ruas" class="table table-striped table-hover-warning" width="100%">
        <thead>
            <tr>
                <td>
                    <?php echo lang('ruass col ruas_id'); ?>
                </td>
                <td>
                    <?php echo lang('ruass col nm_ruas'); ?></a>
                </td>
                <td>
                   <?php echo lang('ruass col no_ruas'); ?></a>
                </td>
                <td>
                   <?php echo lang('ruass col propinsi'); ?></a>
                </td>
                <td>
                    <?php echo lang('ruass col kecamatan'); ?></a>
                </td>
                <td>
                   <?php echo lang('ruass col panjang'); ?></a>
                </td>
                <td>
                   <?php echo lang('ruass col lebar'); ?></a>
                </td>
                <td class="pull-right">
                    <?php echo lang('admin col actions'); ?>
                </td>
            </tr>
        </thead>
        <tbody>

            <?php // data rows ?>
            <?php if ($total) : ?>
                <?php foreach ($ruass as $ruas) : ?>
                    <tr>
                        <td>
                            <?php echo $no; ?>
                        </td>
                        <td>
                            <?php echo $ruas['nm_ruas']; ?>
                        </td>
                        <td>
                            <?php echo $ruas['no_ruas']; ?>
                        </td>
                        <td>
                            <?php echo $ruas['propinsi']; ?>
                        </td>
                        <td>
                            <?php echo $ruas['kecamatan']; ?>
                        </td>
                        <td><?php echo $ruas['panjang']; ?></td>
                        <td><?php echo $ruas['lebar']; ?></td>
                        <td>
                        	<?php $no++; ?>
                            <div class="text-right">
                                <div class="btn-group">
                             
                                        <a href="#modal-<?php echo $ruas['id']; ?>" data-toggle="modal" class="btn btn-danger btn-xs" title="<?php echo lang('admin button delete'); ?>"><span class="glyphicon glyphicon-trash"></span></a>
                             
                                    <a href="<?php echo $this_url; ?>/edit/<?php echo $ruas['id']; ?>" class="btn btn-warning btn-xs" title="<?php echo lang('admin button edit'); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="8">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>

        </tbody>
        <tfoot>
            <tr>
                <th>
                    <?php echo lang('ruass col ruas_id'); ?>
                </th>
                <th>
                    <?php echo lang('ruass col nm_ruas'); ?></a>
                </th>
                <th>
                   <?php echo lang('ruass col no_ruas'); ?></a>
                </th>
                <th>
                   <?php echo lang('ruass col propinsi'); ?></a>
                </th>
                <th>
                    <?php echo lang('ruass col kecamatan'); ?></a>
                </th>
                <th>
                   <?php echo lang('ruass col panjang'); ?></a>
                </th>
                <th>
                   <?php echo lang('ruass col lebar'); ?></a>
                </th>
                <th class="pull-right">
                    <?php echo lang('admin col actions'); ?>
                </th>
            </tr>
        </tfoot>
    </table>
</div>
    <?php // list tools ?>
    <div class="panel-footer">
    </div>

</div>

<?php // delete modal ?>
<?php if ($total) : ?>
    <?php foreach ($ruass as $ruas) : ?>
        <div class="modal fade" id="modal-<?php echo $ruas['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-label-<?php echo $ruas['id']; ?>" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="modal-label-<?php echo $ruas['id']; ?>"><?php echo lang('ruass title ruas_delete');  ?></h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo sprintf(lang('ruass msg delete_confirm'), $ruas['nm_ruas'] . ", " . $ruas['propinsi'] . ", " . $ruas['kota_kab']); ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                        <button type="button" class="btn btn-primary btn-delete-ruas" href="<?php echo site_url('ruass/'); ?>" data-id="<?php echo $ruas['id']; ?>"><?php echo lang('admin button delete'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

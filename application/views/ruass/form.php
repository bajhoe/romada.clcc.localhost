<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php echo form_open('', array('role'=>'form')); ?>

    <?php // hidden id ?>
    <?php if (isset($id)) : ?>
        <?php echo form_hidden('id', $id); ?>
    <?php endif; ?>

    <div class="row">
        <?php // nm_ruas ?>
        <div class="form-group col-sm-4<?php echo form_error('nm_ruas') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input nm_ruas'), 'nm_ruas', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'nm_ruas', 'value'=>set_value('nm_ruas', (isset($ruass['nm_ruas']) ? $ruass['nm_ruas'] : '')), 'class'=>'form-control')); ?>
        </div>
        <?php // no_ruas ?>
        <div class="form-group col-sm-2<?php echo form_error('no_ruas') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input no_ruas'), 'no_ruas', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'no_ruas', 'value'=>set_value('no_ruas', (isset($ruass['no_ruas']) ? $ruass['no_ruas'] : '')), 'class'=>'form-control')); ?>
        </div>
        <?php // tahun_data ?>
        <div class="form-group col-sm-2<?php echo form_error('tahun_data') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input tahun_data'), 'tahun_data', array('class'=>'control-label')); ?>
            <?php echo form_input(array('name'=>'tahun_data', 'value'=>set_value('tahun_data', (isset($ruass['tahun_data']) ? $ruass['tahun_data'] : '')), 'class'=>'form-control')); ?>
        </div>
    </div>

    <div class="row">
        <?php // fungsi ?>
        <div class="form-group col-sm-3<?php echo form_error('fungsi') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input fungsi'), 'fungsi', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'fungsi', 'value'=>set_value('fungsi', (isset($ruass['fungsi']) ? $ruass['fungsi'] : '')), 'class'=>'form-control')); ?>
        </div>

        <?php // status_jalan ?>
        <div class="form-group col-sm-6<?php echo form_error('status_jalan') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input status_jalan'), 'status_jalan', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'status_jalan', 'value'=>set_value('status_jalan', (isset($ruass['status_jalan']) ? $ruass['status_jalan'] : '')), 'class'=>'form-control')); ?>
        </div>
        <?php // kategori ?>
        <div class="form-group col-sm-3<?php echo form_error('kategori') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input kategori'), 'kategori', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'kategori', 'value'=>set_value('kategori', (isset($ruass['kategori']) ? $ruass['kategori'] : '')), 'class'=>'form-control')); ?>
        </div>
    </div>

    <div class="row">
        <?php // propinsi ?>
        <div class="form-group col-sm-3<?php echo form_error('propinsi') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input propinsi'), 'propinsi', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('id'=>'propinsi', 'name'=>'propinsi', 'value'=>set_value('propinsi', (isset($ruass['propinsi']) ? $ruass['propinsi'] : '')), 'class'=>'form-control')); ?>
        </div>
        <?php // kota_kab ?>
        <div class="form-group col-sm-3<?php echo form_error('kota_kab') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input kota_kab'), 'kota_kab', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('id'=>'kota_kab', 'name'=>'kota_kab', 'value'=>set_value('kota_kab', (isset($ruass['kota_kab']) ? $ruass['kota_kab'] : '')), 'class'=>'form-control')); ?>
        </div>
        <?php // kecamatan ?>
        <div class="form-group col-sm-3<?php echo form_error('kecamatan') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input kecamatan'), 'kecamatan', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('id'=>'kecamatan', 'name'=>'kecamatan', 'value'=>set_value('kecamatan', (isset($ruass['kecamatan']) ? $ruass['kecamatan'] : '')), 'class'=>'form-control')); ?>
        </div>
        <?php // desa_kel ?>
        <div class="form-group col-sm-3<?php echo form_error('desa_kel') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input desa_kel'), 'desa_kel', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('id'=>'desa_kel', 'name'=>'desa_kel', 'value'=>set_value('desa_kel', (isset($ruass['desa_kel']) ? $ruass['desa_kel'] : '')), 'class'=>'form-control')); ?>
        </div>
    </div>
    <div class="row">
        <?php // panjang ?>
        <div class="form-group col-sm-4<?php echo form_error('panjang') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input panjang'), 'panjang', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'panjang', 'value'=>set_value('panjang', (isset($ruass['panjang']) ? $ruass['panjang'] : '')), 'class'=>'form-control')); ?>
        </div>
        <?php // lebar ?>
        <div class="form-group col-sm-4<?php echo form_error('lebar') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input lebar'), 'lebar', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'lebar', 'value'=>set_value('lebar', (isset($ruass['lebar']) ? $ruass['lebar'] : '')), 'class'=>'form-control')); ?>
        </div>
        <?php // lhrt ?>
        <div class="form-group col-sm-4<?php echo form_error('lhrt') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('ruass input lhrt'), 'lhrt', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'lhrt', 'value'=>set_value('lhrt', (isset($ruass['lhrt']) ? $ruass['lhrt'] : '')), 'class'=>'form-control')); ?>
        </div>
    </div>
    <?php // buttons ?>
    <div class="row pull-right">
        <a class="btn btn-link" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
        <button type="submit" name="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
    </div>

<?php echo form_close(); ?>

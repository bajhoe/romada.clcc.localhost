<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6 text-left">
                <h3 class="panel-title"><?php //echo lang('pakets title paket_list'); ?></h3>
            </div>
            <div class="col-md-6 text-right">
               <!--  <a class="btn btn-default tooltips btn-sm" href="<?php echo base_url('admin/pakets/add'); ?>" title="<?php echo lang('pakets tooltip add_new_paket') ?>" data-toggle="tooltip"><span class="glyphicon glyphicon-plus-sign"></span> <?php echo lang('pakets button add_new_paket'); ?></a> -->
            </div>
        </div>
    </div>
<div class="table-responsive" style="padding:5px">
    <table class="table table-striped table-hover-warning" id="table-isi" style="width:100%">
        <thead>
            <tr>
                <td>
                    <?php echo lang('pakets col no'); ?>
                </td>
                <td>
                    <?php echo lang('pakets col paket_name'); ?>
                </td>
                <td>
                   <?php echo lang('pakets col jenis_pemel'); ?>
                </td>
                <td>
                  <?php echo lang('pakets col nm_ruas'); ?>
                </td>
                <td>
                  <?php echo lang('pakets col panjang'); ?>
                </td>
                <td>
                  <?php echo lang('pakets col lebar'); ?>
                </td>
                <td>
                  <?php echo lang('pakets col fungsi'); ?>
                </td>
                <td>
                  <?php echo lang('pakets col propinsi'); ?>
                </td>
                <td>
                  <?php echo lang('pakets col kota_kab'); ?>
                </td>
                <td>
                  <?php echo lang('pakets col kecamatan'); ?>
                </td>
                <td style="min-width:100px">
                    <?php echo lang('admin col actions'); ?>
                </td>
            </tr>
        </thead>
        <tbody>

            <?php // data rows ?>
            <?php 
                $no = '';
                (($offset) ? $no = $offset + 1 : $no = 1);
            ?>
            <?php if ($total) : ?>
                <?php foreach ($pakets as $paket) : ?>
                    <tr>
                        <td>
                            <?php echo $no; ?>
                        </td>
                        <td>
                            <?php echo $paket['paket_name']; ?>
                        </td>
                        <td>
                            <?php echo $paket['ref_jenis_pemeliharaan_name']; ?>
                        </td>
                        <td>
                            <?php echo $paket['nm_ruas']; ?>
                        </td>
                        <td>
                            <?php echo $paket['panjang']." km"; ?>
                        </td>
                        <td>
                            <?php echo $paket['lebar']." m"; ?>
                        </td>
                        <td>
                            <?php echo $paket['fungsi']; ?>
                        </td>
                        <td>
                            <?php echo $paket['propinsi']; ?>
                        </td>
                        <td>
                            <?php echo $paket['kota_kab']; ?>
                        </td>
                        <td>
                            <?php echo $paket['kecamatan']; ?>
                        </td>
                        <td>
                        	
                            <div>
                                <div class="btn-group">
                             
                                    <!-- <a href="#modal-<?php echo $paket['id_paket']; ?>" data-toggle="modal" class="btn btn-danger btn-sm" title="<?php echo lang('admin button delete'); ?>">
                                    <span class="glyphicon glyphicon-trash"></span></a>
                                                                  -->
                                   <!--  <a href="<?php echo $this_url; ?>/edit/<?php echo $paket['id_paket']; ?>" class="btn btn-warning btn-sm" title="<?php echo lang('admin button edit'); ?>"><span class="glyphicon glyphicon-pencil"></span></a> -->

                                    <a href="<?php echo site_url('details/stationing'); ?>/<?php echo $paket['id_paket']; ?>" class="btn btn-info btn-sm" title="<?php echo lang('admin button show'); ?>"><span class="glyphicon glyphicon-search"></span></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php $no++; ?>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="7">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>

        </tbody>
        <tfoot>
        </tfoot>
    </table>
</div>
<?php //dtc($pakets); ?>
</div>

<?php // delete modal ?>
<?php if ($total) : ?>
    <?php foreach ($pakets as $paket) : ?>
        <div class="modal fade" id="modal-<?php echo $paket['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-label-<?php echo $paket['id']; ?>" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="modal-label-<?php echo $paket['id']; ?>"><?php echo lang('pakets title paket_delete');  ?></h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo sprintf(lang('pakets msg delete_confirm'), $paket['paket_name'] . " " . $paket['nm_ruas']); ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                        <button type="button" class="btn btn-primary btn-delete-paket" href="<?php echo site_url('admin/pakets/'); ?>" data-id="<?php echo $paket['id']; ?>"><?php echo lang('admin button delete'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

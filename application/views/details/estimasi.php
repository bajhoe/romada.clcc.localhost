<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-default">
    <div class="panel panel-heading">
       <a href="<?php echo site_url('pakets'); ?>" class="btn btn-info btn-sm" title="Return to Project List"><span class="fa fa-arrow-left"></span> Return to Project List</a>
    </div>
<div class="panel panel-body">
 <div class="row">
    <div class="col-md-12"> 
      <!-- Nav tabs -->
      <div class="card">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" ><a href="<?php echo site_url('details/stationing/'); ?><?php echo $paket_id; ?>" aria-controls="stationing"><i class="fa fa-map-pin"></i>  <span>Stations</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/survey/'); ?><?php echo $paket_id; ?>" aria-controls="survey"><i class="fa fa-crosshairs"></i>  <span>Distress Survey</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/pci/'); ?><?php echo $paket_id; ?>" aria-controls="pci"><i class="fa fa-road"></i>  <span>PCI Value</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/rekomendasi/'); ?><?php echo $paket_id; ?>" aria-controls="rekomendasi"><i class="fa fa-wrench"></i>  <span>Treatment Recommendation</span></a></li>
          
          <li role="presentation" class="active"><a href="<?php echo site_url('details/estimasi/'); ?><?php echo $paket_id; ?>" aria-controls="estimasi"><i class="fa fa-list-alt"></i>  <span>Cost Estimation</span></a></li>

          <li role="presentation"><a href="<?php echo site_url('details/rekapitulasi/'); ?><?php echo $paket_id; ?>" aria-controls="rekapitulasi"><i class="fa fa-list"></i>  <span>Summary</span></a></li>
        </ul>

        <!-- Tab panes -->
        <?php //pci ?>
        <div class="tab-content" style="padding:5px;">
            <div role="tabpanel" class="tab-pane active" id="estimasi">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h3 class="panel-title">Information : <?php echo $paket; ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body" > 
<!--                         <div class="row">
<div class="col-md-12">
    <canvas id="thechart"></canvas>
</div>
</div> -->
                        <div class="row">
                        <div class="table-responsive col-md-12" style="padding:5px">
                            
                            <p><b>Total Maintenance Cost Estimation: Rp. <?php echo number_format($total_harga,2,",","."); ?></b></p>
                            <table class="table table-striped table-hover-warning" id="table-estimasi" style="width:100%">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo lang('estimasis col segmen'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('estimasis col distress'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('estimasis col ahspk'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('estimasis col harga_ahspk'); ?>
                                    </th>

                                </tr>
                            </thead>
                            <tbody>
                            <?php // data rows ?>
                               <?php $i=1; ?>
                            <?php foreach ($estimasis as $estimasi) : ?>
                                <tr>
                                    <td>
                                       <b> 
                                        Segmen 
                                        <?php 

                                       if ($estimasi['segmen'] <10){
                                        echo "0";
                                       }
                                       echo $estimasi['segmen']; ?>, dengan Nilai PCI: <?php echo $estimasi['nilai_pci']; ?></b>
                                    </td>
                                    <td>
                                        <?php echo $estimasi['distress_name']." - ".$estimasi['distress_level']." (".$estimasi['tipe'].")"; ?><br>
                                        Area: <?php echo number_format($estimasi['luas'],3,",","."); ?> M<sup>2</sup> <br>
                                        Volume: <?php echo number_format($estimasi['volume'],3,",",".");?> M<sup>3</sup>
                                    </td>
                                     <td>
                                      <ol>
                                          <?php 
                                          if ($estimasi['hsp'] != NULL){
                                            foreach ($estimasi['hsp'] as $row) {
                                                echo "<li>";
                                                echo $row['nama']." | Rp. ".number_format($row['harga'],2,",",".")." per ".unit_sup($row['unit']);   
                                                if ($row['bj'] != NULL){
                                                  echo " | ".number_format($row['bj'],2,",",".")." ".unit_sup($row['unit_bj']);
                                                }
                                                echo "</li>";
                                            }
                                          }

                                          ?>
                                      </ol>
                                    </td>
                                    <td>
                                      <ol>
                                          <?php 
                                          if ($estimasi['hsp'] != NULL){
                                            foreach ($estimasi['hsp'] as $row) {
                                                echo "<li>";
                                                echo "Rp. ".number_format($row['biaya'],2,",",".");
                                                echo "</li>";
                                            }
                                          }

                                          ?>
                                      </ol>   
                                    </td>
                                    <!-- <td>
                                        <div>
                                            <div class="btn-group">
                                         
                                                <a href="#modal-<?php //echo $pci['id_pci']; ?>" data-toggle="modal" class="btn btn-danger btn-sm" title="<?php //echo lang('admin button delete'); ?>">
                                                <span class="glyphicon glyphicon-trash"></span></a>
                                         
                                                <a href="<?php //echo $this_url; ?>/edit/<?php //echo $pci['id_pci']; ?>" class="btn btn-warning btn-sm" title="<?php //echo lang('admin button edit'); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                    
                                            </div>
                                        </div>
                                    </td> -->
                                </tr>
                                <?php $i++; ?>
                                <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                    </div> 
                </div>  
            </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  var nm_paket = "<?php echo $paket; ?>";
  
</script>
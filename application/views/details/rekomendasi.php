<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-default">
    <div class="panel panel-heading">
       <a href="<?php echo site_url('pakets'); ?>" class="btn btn-info btn-sm" title="Return to Project List"><span class="fa fa-arrow-left"></span> Return to Project List</a>
    </div>
<div class="panel panel-body">
 <div class="row">
    <div class="col-md-12"> 
      <!-- Nav tabs -->
      <div class="card">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" ><a href="<?php echo site_url('details/stationing/'); ?><?php echo $paket_id; ?>" aria-controls="stationing"><i class="fa fa-map-pin"></i>  <span>Stations</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/survey/'); ?><?php echo $paket_id; ?>" aria-controls="survey"><i class="fa fa-crosshairs"></i>  <span>Distress Survey</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/pci/'); ?><?php echo $paket_id; ?>" aria-controls="pci"><i class="fa fa-road"></i>  <span>PCI Value</span></a></li>
          
          <li role="presentation"  class="active"><a href="<?php echo site_url('details/rekomendasi/'); ?><?php echo $paket_id; ?>" aria-controls="rekomendasi"><i class="fa fa-wrench"></i>  <span>Treatment Recommendation</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/estimasi/'); ?><?php echo $paket_id; ?>" aria-controls="estimasi"><i class="fa fa-list-alt"></i>  <span>Cost Estimation</span></a></li>
          <li role="presentation"><a href="<?php echo site_url('details/rekapitulasi/'); ?><?php echo $paket_id; ?>" aria-controls="rekapitulasi"><i class="fa fa-list"></i>  <span>Summary</span></a></li>
        </ul>

        <!-- Tab panes -->
        <?php //pci ?>
        <div class="tab-content" style="padding:5px;">
            <div role="tabpanel" class="tab-pane active" id="rekomendasi">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h3 class="panel-title">Information : <?php echo $paket; ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body" > 
<!--                         <div class="row">
<div class="col-md-12">
    <canvas id="thechart"></canvas>
</div>
</div> -->
                        <div class="row">
                        <div class="table-responsive col-md-12" style="padding:5px">
                            <p><b>Total PCI Value: <?php echo $pci_rerata[0]['rerata']; ?></b></p>
                            <!-- <p><b>Total Biaya Pemeliharaan: Rp. <?php //echo number_format($total_harga,2,",","."); ?></b></p> -->
                            <table class="table table-striped table-hover-warning" id="table-rekomendasi" style="width:100%">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo lang('rekomendasis col segmen'); ?>
                                    </th>
                                    <th style="width:200px;">
                                        <?php echo lang('rekomendasis col distress'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('rekomendasis col definition'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('rekomendasis col treatment'); ?>
                                    </th>
<!--                                     <th style="min-width:100px">
    <?php //echo lang('admin col actions'); ?>
</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php // data rows ?>
                               <?php $i=1; ?>
                            <?php foreach ($rekomendasis as $rekomendasi) : ?>
                                <tr>
                                    <td>
                                       <b> 
                                        Segment 
                                        <?php 

                                       if ($rekomendasi['segmen'] <10){
                                        echo "0";
                                       }
                                       echo $rekomendasi['segmen']; ?>, PCI: <?php echo $rekomendasi['nilai_pci']; ?></b>
                                    </td>
                                    <td>
                                        <?php echo $rekomendasi['distress_name']." - ".$rekomendasi['distress_level']." (".$rekomendasi['tipe'].")"; ?> <br>
                                        Area: <?php echo number_format($rekomendasi['luas'],3,".",","); ?> M<sup>2</sup> <br>
                                        Volume: <?php echo number_format($rekomendasi['volume'],3,".",",");?> M<sup>3</sup>
                                    </td>
                                    <td>
                                        
                                        <?php 
                                        echo $rekomendasi['distress_level_def'];
                                        ?> 

                                    </td>
                                    <td>
                                        
                                        <?php 
                                        echo $rekomendasi['distress_level_treat'];
                                        ?> 

                                    </td>
                                   
                                    <!-- <td>
                                        <div>
                                            <div class="btn-group">
                                         
                                                <a href="#modal-<?php //echo $pci['id_pci']; ?>" data-toggle="modal" class="btn btn-danger btn-sm" title="<?php //echo lang('admin button delete'); ?>">
                                                <span class="glyphicon glyphicon-trash"></span></a>
                                         
                                                <a href="<?php //echo $this_url; ?>/edit/<?php //echo $pci['id_pci']; ?>" class="btn btn-warning btn-sm" title="<?php //echo lang('admin button edit'); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                    
                                            </div>
                                        </div>
                                    </td> -->
                                </tr>
                                <?php $i++; ?>
                                <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                    </div> 
                </div>  
            </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
    var nm_paket = "<?php echo $paket; ?>";
</script>
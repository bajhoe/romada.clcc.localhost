<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-default">
    <div class="panel panel-heading">
       <a href="<?php echo site_url('pakets'); ?>" class="btn btn-info btn-sm" title="Return to Project List"><span class="fa fa-arrow-left"></span> Return to Project List</a>
    </div>
<div class="panel panel-body">
 <div class="row">
    <div class="col-md-12"> 
      <!-- Nav tabs -->
      <div class="card">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" ><a href="<?php echo site_url('details/stationing/'); ?><?php echo $paket_id; ?>" aria-controls="stationing"><i class="fa fa-map-pin"></i>  <span>Stations</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/survey/'); ?><?php echo $paket_id; ?>" aria-controls="survey"><i class="fa fa-crosshairs"></i>  <span>Distress Survey</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/pci/'); ?><?php echo $paket_id; ?>" aria-controls="pci"><i class="fa fa-road"></i>  <span>PCI Value</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/rekomendasi/'); ?><?php echo $paket_id; ?>" aria-controls="rekomendasi"><i class="fa fa-wrench"></i>  <span>Treatment Recommendation</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/estimasi/'); ?><?php echo $paket_id; ?>" aria-controls="estimasi"><i class="fa fa-list-alt"></i>  <span>Cost Estimation</span></a></li>

          <li role="presentation" class="active"><a href="<?php echo site_url('details/rekapitulasi/'); ?><?php echo $paket_id; ?>" aria-controls="rekapitulasi"><i class="fa fa-list"></i>  <span>Summary</span></a></li>
        </ul>

        <!-- Tab panes -->
        <?php //pci ?>
        <div class="tab-content" style="padding:5px;">
            <div role="tabpanel" class="tab-pane active" id="rekapitulasi">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h3 class="panel-title">Information : <?php echo $paket; ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body" > 
                        <div class="row"> 
                          <div class="col-md-12 text-center" style="padding:2px">
                          <div class="panel panel-primary"> 
                          <div class="panel-heading"><p class="text-center"><b>Summary</b></p></div> 
                          <div class="panel-body"> 
                            <!-- <p><b>Total PCI Value: <?php //echo $pci_rerata[0]['rerata']; ?></b></p> -->
                            <p><b>Total Maintenance Cost Estimation: Rp. <?php echo number_format($total_harga,2,".",","); ?></b></p>
                          </div>
                          </div>
                          </div>
                        </div>
<!-- row1 -->
                        <div class="row">
                        <div class="col-md-6" style="padding:2px">
                        <div class="panel panel-danger">  
                        <div class="panel-heading"><p class="text-center"><b>Cost Estimation by Segment</b></p></div>
                        <div class="panel-body">
                          <div class="col-md-12" style="padding:2px;">
                          <canvas id="chart1"></canvas>
                        </div>
                        <div class="col-md-12" style="padding:2px;">
                            <table class="table table-responsive table-striped table-hover-warning" id="table-rekapitulasi-1" style="width:100%">
                            <thead>
                                <tr>
                                  <th></th>
                                    <th>
                                        <?php echo lang('rekapitulasis col segmen'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('estimasis col pci'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('rekapitulasis col harga_ahspk'); ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php // data rows ?>
                               <?php $j=1; ?>
                            <?php foreach ($rekaps['segmen'] as $rekap) : ?>
                                <tr>
                                  <td><?php //echo $j; ?></td>
                                    <td>
                                       <b> 
                                        Segment 
                                        <?php 

                                       if ($rekap['segmen'] <10){
                                        echo "0";
                                       }
                                       echo $rekap['segmen']; ?></b>
                                    </td>
                                    <td>
                                      <?php echo $rekap['pci']; ?>
                                    </td>
                                    <td>
                                          <?php 
                                          if ($rekap['biaya'] != NULL){
                                              echo number_format($rekap['biaya'],2,",",".");  
                                            }
                                          ?>
                                    </td>
                                </tr>
                                <?php $j++; ?>
                                <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                        </div>
                        </div>          
<!-- row2 -->
                        <div class="col-md-6" style="padding:2px;">
                        <div class="panel panel-info">  
                          <div class="panel-heading"><p class="text-center"><b>Cost Estimation by Distresess</b></p></div>
                        <div class="panel-body">
                          <div class="col-md-12" style="padding:2px;">
                          <canvas id="chart2"></canvas>
                        </div>
                        <div class="col-md-12" style="padding:2px;">
                            <table class="table table-responsive table-striped table-hover-warning" id="table-rekapitulasi-2" style="width:100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>
                                        <?php echo lang('rekapitulasis col distress'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('rekapitulasis col jml_distress'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('rekapitulasis col harga_ahspk'); ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php // data rows ?>
                               <?php $i=1; ?>
                            <?php foreach ($rekaps['distress'] as $rekap) : ?>
                                <tr>
                                    <td></td>
                                    <td>
                                        <?php echo $rekap['distress']; ?>
                                    </td>
                                    <td>
                                        <?php echo $rekap['jml_distress']; ?>
                                    </td>
                                    <td>
                                          <?php 
                                          if ($rekap['biaya'] != NULL){
                                                echo number_format($rekap['biaya'],2,",",".");
                                          }
                                          ?>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                            </div>
                        </div>
                        </div>
                        </div>
                        </div>
                    </div> 
                </div>  
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
var charting1 = <?php echo json_encode($rekaps['segmen']); ?>;
var charting2 = <?php echo json_encode($rekaps['distress']); ?>;
var nm_paket = "<?php echo $paket; ?>";
</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-default">
    <div class="panel panel-heading">
       <a href="<?php echo site_url('pakets'); ?>" class="btn btn-info btn-sm" title="Return to Project List"><span class="fa fa-arrow-left"></span> Return to Project List</a>
    </div>
<div class="panel panel-body">
 <div class="row">
    <div class="col-md-12"> 
      <!-- Nav tabs -->
      <div class="card">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" ><a href="<?php echo site_url('details/stationing/'); ?><?php echo $paket_id; ?>" aria-controls="stationing"><i class="fa fa-map-pin"></i>  <span>Stations</span></a></li>
          
          <li role="presentation" class="active"><a href="<?php echo site_url('details/survey/'); ?><?php echo $paket_id; ?>" aria-controls="survey"><i class="fa fa-crosshairs"></i>  <span>Distress Survey</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/pci/'); ?><?php echo $paket_id; ?>" aria-controls="pci"><i class="fa fa-road"></i>  <span>PCI Value</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/rekomendasi/'); ?><?php echo $paket_id; ?>" aria-controls="rekomendasi"><i class="fa fa-wrench"></i>  <span>Treatment Recommendation</span></a></li>
          
          <li role="presentation"><a href="<?php echo site_url('details/estimasi/'); ?><?php echo $paket_id; ?>" aria-controls="estimasi"><i class="fa fa-list-alt"></i>  <span>Cost Estimation</span></a></li>

          <li role="presentation"><a href="<?php echo site_url('details/rekapitulasi/'); ?><?php echo $paket_id; ?>" aria-controls="rekapitulasi"><i class="fa fa-list"></i>  <span>Summary</span></a></li>
        </ul>

        <!-- Tab panes -->
        <?php //survey ?>
        <div class="tab-content" style="padding:5px;">
          <div role="tabpanel" class="tab-pane active" id="survey">
                <div class="row">
                <div class="col-md-6" style="padding:5px;"> 
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <h3 class="panel-title">Survey Map  <?php echo $paket; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">       
                                <div id="map"></div>
                            </div>
                        </div>   
                </div>

                <div id="summary-survey" class="col-md-6" style="padding:5px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <h3 class="panel-title">Information : <?php echo $paket; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" > 
                               <div class="col-md-6" style=""> 
 <ol>
<?php foreach ($kerusakans_grup as $rusak): ?>
    <li><?php echo $rusak['distress_name'] .":". $rusak['jml']; ?></li>
<?php endforeach ?>

                            </ol>
    

</div> 
                                <div class="col-md-6">
                            
                           
<canvas id="chart" style="width:500px; height:500px;"></canvas>
</div>
                            </div>
                        </div>
                </div>
                <div class="clearfix"></div>
                </div>
                <div class="table-responsive col-md-12" style="padding:5px">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <div class="panel-title">Distress Table  <?php echo $paket; ?> </div>
                                     <button class="btn btn-warning btn-sm" onclick="resetFilter()">Table Reset</button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body"> 
                <table class="table table-striped table-hover-warning" id="table-survey" style="width:100%">
                    <thead>
                        <tr>
                            <th>
                                <?php echo lang('stations col segmen'); ?>
                            </th>
                            <th>
                                <?php echo lang('stations col foto'); ?>
                            </th>
                            <th>
                                <?php echo lang('stations col namker'); ?>
                            </th>
                            <th>
                               <?php echo lang('stations col panjang'); ?>
                            </th>
                            <th>
                               <?php echo lang('stations col lebar'); ?>
                            </th>
                            <th>
                               <?php echo lang('stations col luas'); ?>
                            </th>
                            <th>
                               <?php echo lang('stations col kedalaman'); ?>
                            </th>
                            <th>
                               <?php echo lang('stations col volume'); ?>
                            </th>
                            <th style="min-width:100px">
                                <?php //echo lang('admin col actions'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php // data rows ?>
                       
                            <?php foreach ($kerusakans as $kerusakan) : ?>
                                <tr>
                                    <td>
                                        <?php echo $kerusakan['segmen']; ?>
                                    </td>
                                    <td>
                                        <?php //echo $kerusakan['lat'].",".$kerusakan['lon']; ?>
                                        <img style="width:150px;" src="<?php echo site_url("uploads/"); echo $kerusakan['foto']; ?>.jpg" alt="<?php echo $kerusakan['foto']; ?>">
                                        
                                    </td>
                                    <td>
                                        <?php echo $kerusakan['distress_name']; ?>
                                        <?php preg_match_all('/\d+/', $kerusakan['tipe'], $matches); 
                                        echo "(".$matches[0][0].substr($kerusakan['tipe'],-1).")";
                                        ?>
                                    </td>
                                    <td>
                                        <?php echo number_format($kerusakan['panjang'], 2)." m"  ; ?>
                                    </td>
                                    <td>
                                        <?php echo number_format($kerusakan['lebar'], 2)." m"  ; ?>
                                    </td>
                                    <td>
                                        <?php echo number_format($kerusakan['luas'], 2)." m<sup>2</sup>"  ; ?>
                                    </td>
                                    <td>
                                        <?php echo number_format($kerusakan['kedalaman'], 2)." m"  ; ?>
                                    </td>
                                    <td>
                                        <?php echo number_format($kerusakan['volume'], 2)." m<sup>3</sup>"  ; ?>
                                    </td>
                                    <td>
                                        
                                        <div>
<!--                                             <div class="btn-group">
                                         
    <a href="#modal-<?php echo $kerusakan['id']; ?>" data-toggle="modal" class="btn btn-danger btn-sm" title="<?php echo lang('admin button delete'); ?>">
    <span class="glyphicon glyphicon-trash"></span></a>
                                         
    <a href="<?php echo $this_url; ?>/edit/<?php echo $kerusakan['id']; ?>" class="btn btn-warning btn-sm" title="<?php echo lang('admin button edit'); ?>"><span class="glyphicon glyphicon-pencil"></span></a>

</div>
                                        </div> -->
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                         <tr>
                            <th>
                                <?php echo lang('stations col segmen'); ?>
                            </th>
                            <th>
                               
                            </th>
                            <th>
                             Total Distresses : <?php echo count($kerusakans); ?>
                            </th>
                            <th>
                              
                            </th>                            
                            <th>
                            
                            </th>
                            <th>
                             
                            </th>
                            <th>
                               
                            </th>
                            <th>
                               
                            </th>
                            <th style="min-width:100px">
                              
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            </div>
        </div>
    </div>


        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script>var charting = <?php echo json_encode($kerusakans_grup); ?>;</script>
<script>
    
    var latlong1 = <?php echo convertsegment1($segments); ?> 
    var latlong2 = <?php echo convertsegment2($segments); ?> 
    var nm_paket = "<?php echo $paket; ?>";

</script>
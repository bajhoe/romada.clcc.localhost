<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Autocomplete_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_db = 'ruass';
    }


    function get_ruass_data($jenis, $phrase)
    {
        
        $sql = "
            SELECT DISTINCT {$jenis}
            FROM {$this->_db}
            WHERE {$jenis} like '%".$phrase."%'
        ";


        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }
        return $results;
    }

}

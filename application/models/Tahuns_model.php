<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tahuns_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_db = 'tahuns';
        $this->load->library('session');
    }


    /**
     * Get list of non-deleted tahuns
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all($limit=0, $offset=0, $filters=array(), $sort='tahuns_id', $dir='ASC')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS *
            FROM {$this->_db} 
            WHERE deleted = '0'
        ";

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {
                $value = $this->db->escape('%' . $value . '%');
                $sql .= " AND {$key} LIKE {$value}";
            }
        }

        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }


    /**
     * Get specific tahun
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_tahun($id=NULL)
    {
        if ($id)
        {
            $sql = "
                SELECT *
                FROM {$this->_db}
                WHERE tahuns_id = " . $this->db->escape($id) . "
                    AND deleted = '0'
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    }


    /**
     * Add a new tahun
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_tahun($data=array())
    {
        if ($data)
        {
            $sql = "
                INSERT INTO {$this->_db} (
                    tahuns_name,
                    status,
                    deleted
                ) VALUES (
                    " . $this->db->escape($data['tahun']) . ",
                    " . $this->db->escape($data['status']) . ",
                    '0'
                )
            ";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Edit an existing tahun
     *
     * @param  array $data
     * @return boolean
     */
    function edit_tahun($data=array())
    {
        if ($data)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                    tahuns_name = " . $this->db->escape($data['tahun']) . ",
                    status = " . $this->db->escape($data['status']) . "
                WHERE tahuns_id = " . $this->db->escape($data['id']) . "
                    AND deleted = '0'
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Soft delete an existing tahun
     *
     * @param  int $id
     * @return boolean
     */
    function delete_tahun($id=NULL)
    {
        if ($id)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                    status = '0',
                    deleted = '1'
                WHERE tahuns_id = " . $this->db->escape($id);

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

/**
     * Get tahuns
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_tahuns()
    {
            $this->db->select('tahuns_id, tahuns_name');
            $this->db->from($this->_db);
            $this->db->order_by('tahuns_id','asc');
            $this->db->where('deleted',0);
            $this->db->where('status',1);
            $query = $this->db->get();
            $loc = array();
            if ($query->num_rows())
            {
                $loc[''] = 'Choose Fiscal Year';
                foreach($query->result_array() as $row) {
                $loc[$row['tahuns_id']] = $row['tahuns_name'];
                }
                return $loc;
            }
        

        return FALSE;
    }


/**
     * Get tahuns for role
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_tahunsforrole()
    {
            $this->db->select('tahuns_id, tahuns_name');
            $this->db->from($this->_db);
            $this->db->order_by('tahuns_id','asc');
            $this->db->where('deleted',0);
            $this->db->where('status',1);
            //echo var_dump($this->session->userdata());
/*            if (!$this->user['is_admin']){
                $ids= array('1','2','3','101','174','175');
                $this->db->where_not_in('tahuns_id', $ids);
            }*/
            $query = $this->db->get();
            $loc = array();
            //echo var_dump($query->result_array());
            if ($query->num_rows())
            {
                $loc = $query->result_array();
                return $loc;
            }
        else
        {
            $loc = NULL;
            return $loc;
        }
    }

    /**
     * Check to see if a tahun already exists
     *
     * @param  string $tahun
     * @return boolean
     */
    function tahun_exists($tahun)
    {
        $sql = "
            SELECT tahuns_id
            FROM {$this->_db}
            WHERE tahuns_name = " . $this->db->escape($tahun) . "
            AND deleted = '0'
            LIMIT 1
        ";

        $query = $this->db->query($sql);

        if ($query->num_rows())
        {
            return TRUE;
        }

        return FALSE;
    }

}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Details_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;
    private $_db2;
    private $_db3;
    private $_db4;
    private $_db5;
    private $_db6;
    private $_db7;
    private $_db8;
    private $_db9;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        // Ambil data session tahun
        $this->load->library('session');
        $this->pengguna = $this->session->userdata('logged_in');
        // define primary table
        $this->_db  = 'tbl_stations';
        $this->_db2 = 'tbl_segments';
        $this->_db3 = 'tbl_kerusakans';
        $this->_db4 = 'tbl_pcis';
        $this->_db5 = 'ref_pci_distresses';
        $this->_db6 = 'ref_divisi_ahspk';
        $this->_db7 = 'ref_seksi_ahspk';
        $this->_db8 = 'tbl_distress_ahspk';
        $this->_db9 = 'tbl_rekapitulasi';

    }


    /**
     * Get list of non-deleted users
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all($limit=0, $offset=0, $filters=array(), $sort='last_name', $dir='ASC')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.*, r.*, j.ref_jenis_pemeliharaan_name
            FROM {$this->_db} p
            LEFT JOIN {$this->_db2} r on p.ref_jalan_id = r.id
            LEFT JOIN {$this->_db3} j on p.ref_jenis_pemeliharaan_id = j.id
            WHERE p.deleted = 0
        ";

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {
                $value = $this->db->escape('%' . $value . '%');
                $sql .= " AND {$key} LIKE {$value}";
            }
        }

        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }


function get_ahspk()
    {
        $sql = "
            SELECT s.*, d.*
            FROM {$this->_db7} s
            LEFT JOIN {$this->_db6} d on s.divisi_ahspk = d.id_divisi
            WHERE s.tahun_ahspk = ".$this->pengguna['tahun']."
            
        ";

        $sql .= " ORDER BY s.divisi_ahspk ASC";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }

    /**
     * Get specific detail
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_detail($id)
    {
        if ($id)
        {
            // CARI Station
            $sql = "
                SELECT *
                FROM {$this->_db} 
                WHERE tbl_pakets = " . $this->db->escape($id);

            $query = $this->db->query($sql);

            if ($query->num_rows() > 0)
            {
                $results['station'] = $query->result_array();
            }
            else
            {
                $results['station'] = NULL;
            }
            // Cari Segment
            $sql2 = "
                    SELECT *
                    FROM {$this->_db2} 
                    WHERE tbl_pakets = " . $this->db->escape($id).
                    "ORDER BY id_segment ASC";

            $query2 = $this->db->query($sql2);

            if ($query2->num_rows() > 0)
            {
                $results['segment'] = $query2->result_array();
            }
            else
            {
                $results['segment'] = NULL;
            }

            // Cari Kerusakan
            $sql3 = "
                    SELECT k.*, d.*
                    FROM {$this->_db3} k
                    LEFT JOIN {$this->_db5} d on k.tipe=d.distress_type
                    WHERE k.tbl_pakets = " . $this->db->escape($id);

            $query3 = $this->db->query($sql3);

            if ($query3->num_rows() > 0)
            {
                $results['kerusakan'] = $query3->result_array();
            }
            else
            {
                $results['kerusakan'] = NULL;
            }

            // Cari Kerusakan with group
            $sql3a = "
                    SELECT d.distress_name, count(k.id) AS jml
                    FROM {$this->_db3} k
                    LEFT JOIN {$this->_db5} d on k.tipe=d.distress_type
                    WHERE k.tbl_pakets = " . $this->db->escape($id)."
                    GROUP BY d.distress_name ORDER BY jml DESC, d.distress_name DESC "
                    ;

            $query3a = $this->db->query($sql3a);

            if ($query3a->num_rows() > 0)
            {
                $results['kerusakan_grup'] = $query3a->result_array();
            }
            else
            {
                $results['kerusakan_grup'] = NULL;
            }

            // Cari PCI
            $sql4 = "
                    SELECT *
                    FROM {$this->_db4} 
                    WHERE tbl_pakets = " . $this->db->escape($id);

            $query4 = $this->db->query($sql4);

            //cari PCI rata
            $sql4a = "
                    SELECT AVG(nilai_pci) AS rerata
                    FROM {$this->_db4} 
                    WHERE tbl_pakets = " . $this->db->escape($id);

            $query4a = $this->db->query($sql4a);


            if ($query4->num_rows() > 0)
            {
                $results['pci'] = $query4->result_array();
                $results['pci_rerata'] = $query4a->result_array();
            }
            else
            {
                $results['pci'] = NULL;
                $results['pci_rerata'] = NULL;
            }

            // Cari Rekomendasi
            $sql5 = "
                    SELECT d.distress_name, k.segmen, d.distress_level_def, d.distress_level_treat, d.distress_level, p.nilai_pci, k.panjang, k.lebar, k.kedalaman, k.tipe, k.luas, k.volume
                    FROM {$this->_db3} k
                    LEFT JOIN {$this->_db4} p on k.segmen=p.tbl_segments
                    LEFT JOIN {$this->_db5} d on k.tipe=d.distress_type
                    WHERE k.tbl_pakets = " . $this->db->escape($id)." AND p.tbl_pakets = " . $this->db->escape($id)." 
                    ORDER BY k.segmen ASC "
                    ;

            $query5 = $this->db->query($sql5);

            if ($query5->num_rows() > 0)
            {
                $results['rekomendasi'] = $query5->result_array();
            }
            else
            {
                $results['rekomendasi'] = NULL;
            }

            // Cari Estimasi
            $sql6 = "
                    SELECT d.distress_name, k.segmen, d.distress_level_def, d.distress_level_treat, d.distress_level, p.nilai_pci, k.panjang, k.lebar, k.kedalaman, k.tipe, k.luas, k.volume
                    FROM {$this->_db3} k
                    LEFT JOIN {$this->_db4} p on k.segmen=p.tbl_segments
                    LEFT JOIN {$this->_db5} d on k.tipe=d.distress_type
                    WHERE k.tbl_pakets = " . $this->db->escape($id)." AND p.tbl_pakets = " . $this->db->escape($id)." 
                    ORDER BY k.segmen ASC "
                    ;

            $query6 = $this->db->query($sql6);

            if ($query6->num_rows() > 0)
            {
                $results['estimasi'] = $query6->result_array();
            }
            else
            {
                $results['estimasi'] = NULL;
            }

            // Cari Rekapitulasi
            $sql7 = "
                    SELECT d.distress_name, k.segmen, d.distress_level_def, d.distress_level_treat, d.distress_level, p.nilai_pci, k.panjang, k.lebar, k.kedalaman, k.tipe, k.luas, k.volume
                    FROM {$this->_db3} k
                    LEFT JOIN {$this->_db4} p on k.segmen=p.tbl_segments
                    LEFT JOIN {$this->_db5} d on k.tipe=d.distress_type
                    WHERE k.tbl_pakets = " . $this->db->escape($id)." AND p.tbl_pakets = " . $this->db->escape($id)." 
                    ORDER BY k.segmen ASC "
                    ;

            $query7 = $this->db->query($sql7);

            if ($query7->num_rows() > 0)
            {
                $results['rekapitulasi'] = $query7->result_array();
            }
            else
            {
                $results['rekapitulasi'] = NULL;
            }


            return $results;
        }

        else
        {
            return FALSE;
        }



}   


    /**
     * Soft delete an existing detail
     *
     * @param  int $id
     * @return boolean
     */
    function delete_detail($id=NULL)
    {
        if ($id)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                    status = '0',
                    deleted = '1'
                WHERE id = " . $this->db->escape($id);

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

/*    function create_table($paket,$tahun){
         $this->load->dbforge();
         $fields = array(
                                'id' => array(
                                                         'type' => 'INT',
                                                         'constraint' => 5,
                                                         'unsigned' => TRUE,
                                                         'auto_increment' => TRUE
                                                  ),
                                'segmen' => array(
                                                         'type' => 'int',
                                                         'constraint' => '10'
                                                  ),
                                'distress' => array(
                                                         'type' =>'VARCHAR',
                                                         'constraint' => '60'
                                                  ),
                                'biaya' => array(
                                                         'type' => 'DOUBLE',
                                                         'constraint' => '15,2',
                                                         'null' => TRUE                           
                                                  ),
                        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        if ($this->dbforge->create_table('rekap_'.$paket.'_'.$tahun, TRUE)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }*/

    function add_rekapitulasi($paket,$tahun,$data){
        $tgl = date('Y-m-d H:i:s');
        if ($data != NULL) {
            foreach ($data as $row){
            $hasil = array(
            'paket'    => $paket,
            'tahun'    => $tahun,
            'segmen'   => $row['segmen'],
            'pci'      => $row['nilai_pci'],
            'distress' => $row['distress_name'],
            'biaya'    => $row['hsp'],
            'created'  => $tgl
            );
            $this->db->insert($this->_db9,$hasil);
            } 
        return TRUE;
        } else {
        return FALSE;
        }
    }

    function delete_rekapitulasi($paket,$tahun){
        $this->db->where('paket', $paket);
        $this->db->where('tahun', $tahun);
        
        if ($this->db->delete($this->_db9)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_rekap_segmen($paket = NULL,$tahun = NULL){
        
        if ($paket != NULL && $tahun != NULL) {
        $this->db->where('paket', $paket);
        $this->db->where('tahun', $tahun);
        }
        $this->db->select('segmen, pci, SUM(biaya) AS biaya');
        $this->db->group_by('segmen');
        $this->db->order_by('segmen', 'ASC');
        $query = $this->db->get($this->_db9);

        if ($query->num_rows() > 0)
        {
            $results = $query->result_array();
        }
        else
        {
            $results= NULL;
        }
        return $results;
    }

    function get_rekap_distress($paket = NULL,$tahun = NULL){
        
        if ($paket != NULL && $tahun != NULL) {
        $this->db->where('paket', $paket);
        $this->db->where('tahun', $tahun);
        }
        $this->db->select('distress, COUNT(distress) AS jml_distress, SUM(biaya) AS biaya');
        $this->db->group_by('distress');
        $this->db->order_by('distress', 'ASC');
        $query = $this->db->get($this->_db9);

        if ($query->num_rows() > 0)
        {
            $results = $query->result_array();
        }
        else
        {
            $results= NULL;
        }
        return $results;
    }

    function get_distress_ahspk($distress)
    {

/*          d.id,
            d.distress_name,
            sa.tahun_ahspk,
            d.distress_type,
            
            */
        $sql = "
            SELECT
            sa.nama_ahspk,
            sa.satuan_ahspk,
            sa.harsat_ahspk,
            sa.bj_ahspk,
            sa.satuan_bj_ahspk
            FROM
            {$this->_db8} da
            LEFT JOIN {$this->_db5} d ON da.id_ref_pci_distress = d.id
            LEFT JOIN {$this->_db7} sa ON da.id_ref_seksi_ahspk = sa.id_ahspk
            WHERE sa.tahun_ahspk = " . $this->pengguna['tahun'] . " 
             AND d.distress_type = " . $this->db->escape($distress) . "
             ORDER BY d.id ASC ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results = $query->result_array();
        }
        else
        {
            $results= NULL;
        }

        return $results;
    }



}

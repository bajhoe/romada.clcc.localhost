<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ruass_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;
    private $_uwong;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // get user logged in data
        if ($this->session->userdata('logged_in')) {
            $this->_uwong = $this->session->userdata('logged_in');
            }

        // define primary table
        $this->_db = 'ruass';
        $this->_db2 = 'roles';
        
    }


    /**
     * Get list of non-deleted users
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all($user_id)
    {    
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS *
            FROM {$this->_db}
            ";
        if (!$this->user['is_admin']) {
            $sql .= "WHERE user_id = ".$user_id;    
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }


    /**
     * Get specific ruas
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_ruas($id=NULL)
    {
        if ($id)
        {
            $sql = "
                SELECT *
                FROM {$this->_db} 
                WHERE id = " . $this->db->escape($id) . "

            ";

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    }


    /**
     * Add a new ruas
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_ruas($data=array())
    {
        if ($data)
        {

            $sql = "
                INSERT INTO {$this->_db} (
                nm_ruas,
                no_ruas,
                tahun_data,
                fungsi,
                status_jalan,
                kategori,
                propinsi,
                kota_kab,
                desa_kel,
                kecamatan,
                panjang,
                lebar,
                lhrt,
                user_id
                ) VALUES (
                    " . $this->db->escape($data['nm_ruas']) . ",
                    " . $this->db->escape($data['no_ruas']) . ",
                    " . $this->db->escape($data['tahun_data']) . ",
                    " . $this->db->escape($data['fungsi']) . ",
                    " . $this->db->escape($data['status_jalan']) . ",
                    " . $this->db->escape($data['kategori']) . ",
                    " . $this->db->escape($data['propinsi']) . ",
                    " . $this->db->escape($data['kota_kab']) . ",
                    " . $this->db->escape($data['desa_kel']) . ",
                    " . $this->db->escape($data['kecamatan']) . ",
                    " . $this->db->escape($data['lebar']) . ",
                    " . $this->db->escape($data['kota_kab']) . ",
                    " . $this->db->escape($data['lhrt']) . ",
                    " . $this->_uwong['id'] . "
                )";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Edit an existing user
     *
     * @param  array $data
     * @return boolean
     */
    function edit_ruas($data=array())
    {
        if ($data)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                nm_ruas = " . $this->db->escape($data['nm_ruas']) . ",
                no_ruas = " . $this->db->escape($data['no_ruas']) . ",
                tahun_data = " . $this->db->escape($data['tahun_data']) . ",
                fungsi = " . $this->db->escape($data['fungsi']) . ",
                status_jalan = " . $this->db->escape($data['status_jalan']) . ",
                kategori = " . $this->db->escape($data['kategori']) . ",
                propinsi = " . $this->db->escape($data['propinsi']) . ",
                kota_kab = " . $this->db->escape($data['kota_kab']) . ",
                kecamatan = " . $this->db->escape($data['kecamatan']) . ",
                desa_kel = " . $this->db->escape($data['desa_kel']) . ",
                panjang = " . $this->db->escape($data['panjang']) . ",
                lebar = " . $this->db->escape($data['lebar']) . ",
                lhrt = " . $this->db->escape($data['lhrt']) . "
                WHERE id = " . $this->db->escape($data['id']) . "
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }


    /**
     * delete an existing ruas
     *
     * @param  int $id
     * @return boolean
     */
    function delete_ruas($id=NULL)
    {
        if ($id)
        {
            $sql = "
                DELETE FROM {$this->_db}
                WHERE id = " . $this->db->escape($id);

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }
}

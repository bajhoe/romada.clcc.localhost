<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Distress_ahspks_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;
    private $_db2;
    private $_db3;
    private $_db4;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        // Ambil data session tahun
        $this->load->library('session');
        $this->pengguna = $this->session->userdata('logged_in');
        // define primary table
        $this->_db  = 'ref_pci_distresses';
        $this->_db2 = 'ref_seksi_ahspk';
        $this->_db3 = 'tbl_distress_ahspk';
        $this->_db4 = 'ref_divisi_ahspk';
    }


    /**
     * Get list of distresses
     */
    function get_distresses()
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS d.*
            FROM {$this->_db} d
            
        ";

        $sql .= " ORDER BY id";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }

    /**
     * Get list of ahspks
     */
    function get_ahspks($data=NULL)
    {
        $sql = "
            SELECT s.*, d.*
            FROM {$this->_db2} s
            LEFT JOIN {$this->_db4} d on s.divisi_ahspk = d.id_divisi
            WHERE s.tahun_ahspk = ".$this->pengguna['tahun']."
            
        ";
        if ($data!=NULL){
        $sql .= " AND s.id_ref_seksi_ahspk NOT IN (".implode($data).") ";
        }
        $sql .= " ORDER BY s.divisi_ahspk ASC";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }

    /**
     * Get list of distresses_ahspks
     */
    function get_distress_ahspks($distress=NULL)
    {
        $sql = "";
        if ($distress != NULL) {
        $sql .= "
            SELECT SQL_CALC_FOUND_ROWS d.id_ref_seksi_ahspk
            FROM {$this->_db3} d
            WHERE d.tahun_ahspk = " . $this->pengguna['tahun'] . "
             AND d.id_ref_pci_distress = " . $this->db->escape($distress) . " ";
        } else {
        $sql .= "
            SELECT SQL_CALC_FOUND_ROWS d.*, dt.*, ah.*
            FROM {$this->_db3} d
            LEFT JOIN {$this->_db} dt on d.id_ref_pci_distress = dt.id
            LEFT JOIN {$this->_db2} ah on d.id_ref_seksi_ahspk = ah.id_ahspk
            WHERE d.tahun_ahspk = " . $this->pengguna['tahun'] . "
        ";
        } 
        
        $sql .= " ORDER BY d.id_ref_seksi_ahspk";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }

    /**
     * Add a new ruas
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add($data=array())
    {
        if ($data)
        {

            $sql = "
                INSERT INTO {$this->_db3} (
                    id_ref_pci_distress,
                    id_ref_seksi_ahspk,
                    tahun_ahspk
                ) VALUES (
                    " . $this->db->escape($data['id_ref_pci_distress']) . ",
                    " . $this->db->escape($data['id_ref_seksi_ahspk']) . ",
                    " . $this->db->escape($data['tahun_ahspk']) . "
                )
            ";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Delete an existing data
     *
     * @param  int $id
     * @return boolean
     */
    function delete_distress_ahspks($id=NULL)
    {
        if ($id)
        {
            $sql = "
                DELETE FROM {$this->_db3}
                WHERE id_distress_ahspk = " . $this->db->escape($id);

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Roles_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;
    private $_db2;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_db = 'roles';
        $this->_db2 = 'tahaps';
    }


    /**
     * Get list of non-deleted roless
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all($limit = 0, $offset = 0, $filters = array(), $sort = 'roles_id', $dir = 'ASC')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS *
            FROM {$this->_db}
            WHERE deleted = 0
        ";

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {
                $value = $this->db->escape('%' . $value . '%');
                $sql .= " AND {$key} LIKE {$value}";
            }
        }

        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }


    /**
     * Get specific roles
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_role($id = NULL)
    {
        if ($id)
        {
            $sql = "
                SELECT *
                FROM {$this->_db}
                WHERE roles_id = " . $this->db->escape($id) . "
                    AND deleted = '0'
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    }


    /**
     * Add a new role
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_role($data = array())
    {
        if ($data)
        {

            $sql = "
                INSERT INTO {$this->_db} 
                (
                    roles_name,
                    status
                ) 
                VALUES 
                (
                    " . $this->db->escape($data['roles_name']) . ",
                    " . $this->db->escape($data['status']) . "
                )
            ";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }


    /**
     * Edit an existing role
     *
     * @param  array $data
     * @return boolean
     */
    function edit_role($data = array())
    {
        if ($data)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                    roles_name = " . $this->db->escape($data['roles_name']) . ",
                    status = " . $this->db->escape($data['status']) . "
                    WHERE roles_id = " . $this->db->escape($data['roles_id']);

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Soft delete an existing roles
     *
     * @param  int $id
     * @return boolean
     */
    function delete_role($id = NULL)
    {
        if ($id)
        {
            $sql = "

                UPDATE {$this->_db}
                SET
                    deleted = 1
                WHERE roles_id = " . $this->db->escape($id);

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

	/**
     * Duplicate an existing roles
     *
     * @param  array $data
     * @return boolean
     */
    function duplicate_role($id = NULL)
    {
        if ($id)
        {
            $sql = "
			INSERT INTO {$this->_db}(roles_name
									 ) 
			SELECT  roles_name
			FROM {$this->_db} 
			WHERE roles_id = " . $this->db->escape($id);
			$this->db->query($sql);
			
         if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }
        return FALSE;
    }
	
	/**
     * Get roles
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_roles()
    {
			$this->db->select('roles_id, roles_name');
			$this->db->from($this->_db);
			$this->db->order_by('roles_id','asc');
            $this->db->where('deleted',0);
            $this->db->where('status',1);
            
			$query = $this->db->get();
			$loc = array();
            if ($query->num_rows())
            {
				//$loc[''] = '';
				foreach($query->result_array() as $row) {
				$loc[$row['roles_id']] = $row['roles_name'];
				}
				return $loc;
            }
        

        return FALSE;
    }

/**
     * Get roles by role
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_roles_by_role($roles=NULL)
    {
            $this->db->select('roles_id, roles_name');
            $this->db->from($this->_db);
            $this->db->order_by('roles_id','asc');
            $this->db->where('deleted',0);
            $this->db->where('status',1);

            if ($roles) {
                $this->db->where_in('roles_id', explode(',',$roles));
            }
            $query = $this->db->get();
            $loc = array();
            if ($query->num_rows())
            {
                $loc[''] = '';
                foreach($query->result_array() as $row) {
                $loc[$row['roles_id']] = $row['roles_name'];
                }
                return $loc;
            }
        

        return FALSE;
    }



/**
     * Get rolestahap
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_rolestahap($roles)
    {
            $this->db->select('tahaps_id, tahaps_name');
            $this->db->from($this->_db2);
            $this->db->order_by('tahaps_id','asc');
            $this->db->where('deleted',0);
            $this->db->where('status',1);
            //echo var_dump($this->session->userdata());
        if ($roles){
                //$role= array('1','2','3','101','174','175');
                $this->db->where_in('tahaps_id', $roles);
            }
            $query = $this->db->get();
            $loc = array();
            if ($query->num_rows())
            {
                $loc[''] = '';
                foreach($query->result_array() as $row) {
                $loc[$row['tahaps_id']] = $row['tahaps_name'];
                }
                return $loc;
            }
        

        return FALSE;
    }
	
    /**
     * Check to see if a roles_name already exists
     *
     * @param  string $roles_name
     * @return boolean
     */
    function role_exists($roles_name)
    {
        $sql = "
            SELECT roles_id
            FROM {$this->_db}
            WHERE roles_name = " . $this->db->escape($roles_name) . "
            AND deleted = '0'
            LIMIT 1
        ";

        $query = $this->db->query($sql);

        if ($query->num_rows())
        {
            return TRUE;
        }

        return FALSE;
    }

}

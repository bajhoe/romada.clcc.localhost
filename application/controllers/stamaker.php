<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Stamaker extends Private_Controller {

    /**
     * @var string
     */
    private $_redirect_url;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->pengguna = $this->session->userdata('logged_in');

        // load the language files
        $this->lang->load('details');
        $this->lang->load('admin');
        $this->load->helper('format');

        // load the ruass model
        $this->load->model('details_model');
        $this->load->model('pakets_model');
        $this->load->model('ruass_model');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('details'));

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Buat STA marker
     */
    function maker($ruas)
    {
        // get list
        //$details = $this->details_model->get_detail($ruas);
        //$ruas_id = $this->pakets_model->get_ruas_by_paket($ruas);
        //$keterangan = $this->ruass_model->get_ruas($ruas_id['ref_jalan_id']);

        // setup page header data
		$this
            ->add_css_theme(array(
                                  'leaflet.css',
                                  'map.css',
                                  'datatables.min.css',
                                  'responsive.dataTables.min.css'
                              ))
			

            ->add_js_theme(array('leaflet.js', 
                                 'leaflet-ant-path.js',
                                 'datatables.min.js',
                                 'dataTables.responsive.min.js',
                                 'stamaker.js'
                             ))
			->set_title(lang('details title detail_stationing'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'ruas'       => $keterangan,
            'stations'   => $details['station'],
            'segments'   => $details['segment'],
            'paket'      => $ruas_id['paket_name'],
            'paket_id'   => $ruas,
        );
        //dtc($details['results']);
        // load views
        $data['content'] = $this->load->view('stamaker', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
}

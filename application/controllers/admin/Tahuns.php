<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tahuns extends Admin_Controller {

    /**
     * @var string
     */
    private $_redirect_url;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('tahuns');

        // load the tahuns model
        $this->load->model('tahuns_model');


        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/tahuns'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "tahuns_id");
        define('DEFAULT_DIR', "asc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * tahun list page
     */
    function index()
    {
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('tahun'))
        {
            $filters['tahuns_name'] = $this->input->get('tahun', TRUE);
        }


        // build filter string
        $filter = "";
        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('tahun'))
                {
                    $filter .= "&tahun=" . $this->input->post('tahun', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $tahuns = $this->tahuns_model->get_all($limit, $offset, $filters, $sort, $dir);

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $tahuns['total'],
            'per_page'   => $limit
        ));

        // setup page header data
		$this
			->add_js_theme('tahuns_i18n.js', TRUE )
			->set_title(lang('tahuns title tahun_list'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'tahuns'  => $tahuns['results'],
            'total'      => $tahuns['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir
        );

        // load views
        $data['content'] = $this->load->view('admin/tahuns/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Add new tahun
     */
    function add()
    {
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('tahun', lang('tahuns input tahun'), 'required|trim|callback__check_tahun[]');


        if ($this->form_validation->run() == TRUE)
        {
            // save the new tahun
            $saved = $this->tahuns_model->add_tahun($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('tahuns msg add_tahun_success'), $this->input->post('tahun')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('tahuns error add_tahun_failed'), $this->input->post('tahun')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this->set_title(lang('tahuns title tahun_add'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'tahun'          => NULL
        );

        // load views
        $data['content'] = $this->load->view('admin/tahuns/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Edit existing tahun
     *
     * @param  int $id
     */
    function edit($id=NULL)
    {
        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $tahun = $this->tahuns_model->get_tahun($id);

        // if empty results, return to list
        if ( ! $tahun)
        {
            redirect($this->_redirect_url);
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('tahun', lang('tahuns input tahun'), 'required|trim');

        if ($this->form_validation->run() == TRUE)
        {
            // save the changes
            $saved = $this->tahuns_model->edit_tahun($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('tahuns msg edit_tahun_success'), $this->input->post('tahun')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('tahuns error edit_tahun_failed'), $this->input->post('tahun')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this->set_title(lang('tahuns title tahun_edit'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'tahun'              => $tahun,
            'tahun_id'           => $id
        );

        // load views
        $data['content'] = $this->load->view('admin/tahuns/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Delete a tahun
     *
     * @param  int $id
     */
    function delete($id=NULL)
    {
        // make sure we have a numeric id
        if ( ! is_null($id) OR ! is_numeric($id))
        {
            // get tahun details
            $tahun = $this->tahuns_model->get_tahun($id);

            if ($tahun)
            {
                // soft-delete the tahun
                $delete = $this->tahuns_model->delete_tahun($id);

                if ($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('tahuns msg delete_tahun'), $tahun['tahuns_name']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('tahuns error delete_tahun'), $tahun['tahuns_name']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('tahuns error tahun_not_exist'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('tahuns error tahun_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }


    /**
     * Export list to CSV
     */
    function export()
    {
        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();


        if ($this->input->get('tahun'))
        {
            $filters['tahuns_name'] = $this->input->get('tahun', TRUE);
        }

        // get all tahuns
        $tahuns = $this->tahuns_model->get_all(0, 0, $filters, $sort, $dir);

        if ($tahuns['total'] > 0)
        {
            // manipulate the output array
            foreach ($tahuns['results'] as $key=>$tahun)
            {
                unset($tahuns['results'][$key]['password']);
                unset($tahuns['results'][$key]['deleted']);

                if ($tahun['status'] == 0)
                {
                    $tahuns['results'][$key]['status'] = lang('admin input inactive');
                }
                else
                {
                    $tahuns['results'][$key]['status'] = lang('admin input active');
                }
            }

            // export the file
            array_to_csv($tahuns['results'], "tahuns");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }


    /**************************************************************************************
     * PRIVATE VALIDATION CALLBACK FUNCTIONS
     **************************************************************************************/


    /**
     * Make sure tahun is available
     *
     * @param  string $tahun
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_tahun($tahun, $current)
    {
        if (trim($tahun) != trim($current) && $this->tahuns_model->tahun_exists($tahun))
        {
            $this->form_validation->set_message('_check_tahun', sprintf(lang('tahuns error tahun_exists'), $tahun));
            return FALSE;
        }
        else
        {
            return $tahun;
        }
    }

}

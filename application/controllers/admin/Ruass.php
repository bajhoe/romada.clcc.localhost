<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ruass extends Admin_Controller {

    /**
     * @var string
     */
    private $_redirect_url;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('ruass');

        // load the ruass model
        $this->load->model('ruass_model');

        // load the roles model
       // $this->load->model('roles_model');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/ruass'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "nm_ruas");
        define('DEFAULT_DIR', "asc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * User list page
     */
    function index()
    {
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('no_ruas'))
        {
            $filters['no_ruas'] = $this->input->get('no_ruas', TRUE);
        }

        if ($this->input->get('nm_ruas'))
        {
            $filters['nm_ruas'] = $this->input->get('nm_ruas', TRUE);
        }

        if ($this->input->get('kecamatan'))
        {
            $filters['kecamatan'] = $this->input->get('kecamatan', TRUE);
        }

        // build filter string
        $filter = "";
        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('no_ruas'))
                {
                    $filter .= "&no_ruas=" . $this->input->post('no_ruas', TRUE);
                }

                if ($this->input->post('nm_ruas'))
                {
                    $filter .= "&nm_ruas=" . $this->input->post('nm_ruas', TRUE);
                }

                if ($this->input->post('kecamatan'))
                {
                    $filter .= "&kecamatan=" . $this->input->post('kecamatan', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $ruass = $this->ruass_model->get_all($limit, $offset, $filters, $sort, $dir);

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $ruass['total'],
            'per_page'   => $limit
        ));

        // setup page header data
		$this
			->add_js_theme('ruass_i18n.js', TRUE )
			->set_title(lang('ruass title ruas_list'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'ruass'      => $ruass['results'],
            'total'      => $ruass['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir
        );

        // load views
        $data['content'] = $this->load->view('admin/ruass/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Add new ruas
     */
    function add()
    {
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('no_ruas', lang('ruass input no_ruas'), 'required|trim|min_length[5]|max_length[30]|callback__check_no_ruas[]');
        $this->form_validation->set_rules('nm_ruas', lang('ruass input nm_ruas'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('kecamatan', lang('ruass input kecamatan'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('email', lang('ruass input email'), 'required|trim|max_length[128]|valid_email|callback__check_email[]');
        $this->form_validation->set_rules('language', lang('ruass input language'), 'required|trim');
        $this->form_validation->set_rules('status', lang('ruass input status'), 'required|numeric');
        $this->form_validation->set_rules('is_admin', lang('ruass input is_admin'), 'required|numeric');
        $this->form_validation->set_rules('role', lang('ruass input role'), 'required|numeric');
        $this->form_validation->set_rules('password', lang('ruass input password'), 'required|trim|min_length[5]');
        $this->form_validation->set_rules('password_repeat', lang('ruass input password_repeat'), 'required|trim|matches[password]');

        if ($this->form_validation->run() == TRUE)
        {
            // save the new ruas
            $saved = $this->ruass_model->add_ruas($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('ruass msg add_ruas_success'), $this->input->post('nm_ruas') . " " . $this->input->post('kecamatan')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('ruass error add_ruas_failed'), $this->input->post('nm_ruas') . " " . $this->input->post('kecamatan')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this->set_title(lang('ruass title ruas_add'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'ruas'              => NULL
        );

        // load views
        $data['content'] = $this->load->view('admin/ruass/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Edit existing user
     *
     * @param  int $id
     */
    function edit($id=NULL)
    {
        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $user = $this->users_model->get_user($id);

        // if empty results, return to list
        if ( ! $user)
        {
            redirect($this->_redirect_url);
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('no_ruas', lang('users input no_ruas'), 'required|trim|min_length[5]|max_length[30]|callback__check_no_ruas[' . $user['no_ruas'] . ']');
        $this->form_validation->set_rules('nm_ruas', lang('users input nm_ruas'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('kecamatan', lang('users input kecamatan'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[128]|valid_email|callback__check_email[' . $user['email'] . ']');
        $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');
        $this->form_validation->set_rules('status', lang('users input status'), 'required|numeric');
        $this->form_validation->set_rules('is_admin', lang('users input is_admin'), 'required|numeric');
        $this->form_validation->set_rules('role', lang('users input role'), 'required|numeric');
        $this->form_validation->set_rules('password', lang('users input password'), 'min_length[5]|matches[password_repeat]');
        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'matches[password]');

        if ($this->form_validation->run() == TRUE)
        {
            // save the changes
            $saved = $this->users_model->edit_user($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('users msg edit_user_success'), $this->input->post('nm_ruas') . " " . $this->input->post('kecamatan')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('users error edit_user_failed'), $this->input->post('nm_ruas') . " " . $this->input->post('kecamatan')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this->set_title(lang('users title user_edit'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'user'              => $user,
            'roles'             => $this->roles_model->get_roles(),
            'user_id'           => $id,
            'password_required' => FALSE
        );

        // load views
        $data['content'] = $this->load->view('admin/users/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Delete a ruas
     *
     * @param  int $id
     */
    function delete($id=NULL)
    {
        // make sure we have a numeric id
        if ( ! is_null($id) OR ! is_numeric($id))
        {
            // get ruas details
            $ruas = $this->ruass_model->get_ruas($id);

            if ($ruas)
            {
                // soft-delete the ruas
                $delete = $this->ruass_model->delete_ruas($id);

                if ($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('ruass msg delete_ruas'), $ruas['nm_ruas'] . " " . $ruas['kecamatan']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('ruass error delete_ruas'), $ruas['nm_ruas'] . " " . $ruas['kecamatan']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('ruass error ruas_not_exist'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('ruass error ruas_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }


    /**
     * Export list to CSV
     */
    function export()
    {
        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('no_ruas'))
        {
            $filters['no_ruas'] = $this->input->get('no_ruas', TRUE);
        }

        if ($this->input->get('nm_ruas'))
        {
            $filters['nm_ruas'] = $this->input->get('nm_ruas', TRUE);
        }

        if ($this->input->get('kecamatan'))
        {
            $filters['kecamatan'] = $this->input->get('kecamatan', TRUE);
        }

        // get all users
        $users = $this->users_model->get_all(0, 0, $filters, $sort, $dir);

        if ($users['total'] > 0)
        {
            // manipulate the output array
            foreach ($users['results'] as $key=>$user)
            {
                unset($users['results'][$key]['password']);
                unset($users['results'][$key]['deleted']);

                if ($user['status'] == 0)
                {
                    $users['results'][$key]['status'] = lang('admin input inactive');
                }
                else
                {
                    $users['results'][$key]['status'] = lang('admin input active');
                }
            }

            // export the file
            array_to_csv($users['results'], "users");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }


    /**************************************************************************************
     * PRIVATE VALIDATION CALLBACK FUNCTIONS
     **************************************************************************************/


    /**
     * Make sure no_ruas is available
     *
     * @param  string $no_ruas
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_no_ruas($no_ruas, $current)
    {
        if (trim($no_ruas) != trim($current) && $this->users_model->no_ruas_exists($no_ruas))
        {
            $this->form_validation->set_message('_check_no_ruas', sprintf(lang('users error no_ruas_exists'), $no_ruas));
            return FALSE;
        }
        else
        {
            return $no_ruas;
        }
    }


    /**
     * Make sure email is available
     *
     * @param  string $email
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_email($email, $current)
    {
        if (trim($email) != trim($current) && $this->users_model->email_exists($email))
        {
            $this->form_validation->set_message('_check_email', sprintf(lang('users error email_exists'), $email));
            return FALSE;
        }
        else
        {
            return $email;
        }
    }

}

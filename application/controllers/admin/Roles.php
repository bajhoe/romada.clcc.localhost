<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends Admin_Controller {

    /**
     * @var string
     */
    private $_redirect_url;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('roles');

        // load the roles model
        $this->load->model('roles_model');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/roles'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "roles_id");
        define('DEFAULT_DIR', "asc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Roles list page
     */
    function index()
    {
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('role'))
        {
            $filters['roles_name'] = $this->input->get('role', TRUE);
        }

        // build filter string
        $filter = "";
        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('role'))
                {
                    $filter .= "&role=" . $this->input->post('role', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $roles = $this->roles_model->get_all($limit, $offset, $filters, $sort, $dir);

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $roles['total'],
            'per_page'   => $limit
        ));

        // setup page header data
		//$this->add_js_theme('roles_i18n.js', TRUE )
        $this
            ->add_js_theme(array('tokenize2.min.js', 'roles_i18n.js'))
			->set_title(lang('roles title role_list'))
            ->add_css_theme('tokenize2.min.css');

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'roles'      => $roles['results'],
            'total'      => $roles['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir
        );

        // load views
        $data['content'] = $this->load->view('admin/roles/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Add new role
     */
    function add()
    {
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('roles_name', lang('roles input role'), 'required|trim|callback__check_role[]');
        
        if ($this->form_validation->run() == TRUE)
        {
            // save the new role
            $saved = $this->roles_model->add_role($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('roles msg add_role_success'), $this->input->post('roles_name')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('roles error add_role_failed'), $this->input->post('roles_name')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this->set_title(lang('roles title role_add'));

        $this->add_js_theme(array('tokenize2.min.js', 'roles_i18n.js'));
        $this->add_css_theme('tokenize2.min.css');

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'roles'             => NULL
        );

        // load views
        $data['content'] = $this->load->view('admin/roles/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Edit existing role
     *
     * @param  int $id
     */
    function edit($id=NULL)
    {
        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $role = $this->roles_model->get_role($id);
        // if empty results, return to list
        if ( ! $role)
        {
            redirect($this->_redirect_url);
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('roles_name', lang('roles input role'), 'required|trim|callback__check_role[' . $role['roles_name'] . ']');
//echo var_dump( $role['roles_name']);
        if ($this->form_validation->run() == TRUE)
        {
            // save the changes
            $saved = $this->roles_model->edit_role($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('roles msg edit_role_success'), $this->input->post('roles_name')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('roles error edit_role_failed'), $this->input->post('roles_name')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this->set_title(lang('roles title role_edit'));

        $this->add_js_theme(array('tokenize2.min.js', 'roles_i18n.js'));
        $this->add_css_theme('tokenize2.min.css');

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'roles'             => $role,
            'roles_id'          => $id
        );

        // load views
        $data['content'] = $this->load->view('admin/roles/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Delete a role
     *
     * @param  int $id
     */
    function delete($id=NULL)
    {
        // make sure we have a numeric id
        if ( ! is_null($id) OR ! is_numeric($id))
        {
            // get role details
            $role = $this->roles_model->get_role($id);

            if ($role)
            {
                // soft-delete the role
                $delete = $this->roles_model->delete_role($id);

                if ($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('roles msg delete_role'), $role['roles_name']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('roles error delete_role'), $role['roles_name']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('roles error role_not_exist'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('roles error role_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }


    /**
     * Export list to CSV
     */
    function export()
    {
        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('role'))
        {
            $filters['roles_name'] = $this->input->get('role', TRUE);
        }

        // get all users
        $roles = $this->roles_model->get_all(0, 0, $filters, $sort, $dir);

        if ($roles['total'] > 0)
        {
            // manipulate the output array
            foreach ($roles['results'] as $key=>$role)
            {
                unset($roles['results'][$key]['password']);
                unset($roles['results'][$key]['deleted']);

                if ($role['status'] == 0)
                {
                    $roles['results'][$key]['status'] = lang('admin input inactive');
                }
                else
                {
                    $roles['results'][$key]['status'] = lang('admin input active');
                }
            }

            // export the file
            array_to_csv($roles['results'], "roles");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }


    /**************************************************************************************
     * PRIVATE VALIDATION CALLBACK FUNCTIONS
     **************************************************************************************/

    /**
     * Make sure role is available
     *
     * @param  string $role
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_role($role, $current)
    {
        if (trim($role) != trim($current) && $this->roles_model->role_exists($role))
        {
            $this->form_validation->set_message('_check_role', sprintf(lang('roles error role_exists'), $role));
            return FALSE;
        }
        else
        {
            return $role;
        }
    }

}

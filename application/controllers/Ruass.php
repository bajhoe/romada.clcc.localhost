<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ruass extends Private_Controller {

    /**
     * @var string
     */
    private $_redirect_url;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('ruass');
        $this->lang->load('admin');

        // load the ruass model
        $this->load->model('ruass_model');
        $this->load->model('autocomplete_model');


        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('ruass'));

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * ruass list page
     */
    function index()
    {

         // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL);
        // get user logged in data
        if ($this->session->userdata('logged_in')) {
            $uwong = $this->session->userdata('logged_in');
            }
        // get list
        $ruass = $this->ruass_model->get_all($uwong['id']);

        // setup page header data	
        $this
            ->add_css_theme(array(
                                  'map.css',
                                  'datatables.min.css',
                                  'responsive.dataTables.min.css'
                              ))
            ->add_js_theme(array(
                                 'datatables.min.js',
                                 'dataTables.responsive.min.js',
                                 'ruass_i18n.js'
                             ))
            ->set_title(lang('ruass title ruas_list'));
        
        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'ruass'      => $ruass['results'],
            'total'      => $ruass['total'],
            'no'         => 1
        );

        // load views
        $data['content'] = $this->load->view('ruass/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Add new ruas
     */
    function add()
    {
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('no_ruas', lang('ruass input no_ruas'), 'required|trim|min_length[1]|max_length[30]');
        $this->form_validation->set_rules('nm_ruas', lang('ruass input nm_ruas'), 'required|trim|min_length[1]|max_length[32]');
        $this->form_validation->set_rules('tahun_data', lang('ruass input tahun_data'), 'trim|numeric');
        $this->form_validation->set_rules('propinsi', lang('ruass input propinsi'), 'required|trim');
        $this->form_validation->set_rules('kota_kab', lang('ruass input kota_kab'), 'required|trim');
        $this->form_validation->set_rules('kecamatan', lang('ruass input kecamatan'), 'required|trim');
        $this->form_validation->set_rules('desa_kel', lang('ruass input desa_kel'), 'required|trim');
        $this->form_validation->set_rules('panjang', lang('ruass input panjang'), 'required|trim|numeric');
        $this->form_validation->set_rules('lebar', lang('ruass input lebar'), 'required|trim|numeric');
        $this->form_validation->set_rules('fungsi', lang('ruass input fungsi'), 'required|trim');
        $this->form_validation->set_rules('kategori', lang('ruass input kategori'), 'required|trim');
        $this->form_validation->set_rules('status_jalan', lang('ruass input status_jalan'), 'required|trim');

        if ($this->form_validation->run() == TRUE)
        {
            // save the new ruas
            $saved = $this->ruass_model->add_ruas($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('ruass msg add_ruas_success'), $this->input->post('nm_ruas') . " " . $this->input->post('kecamatan')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('ruass error add_ruas_failed'), $this->input->post('nm_ruas') . " " . $this->input->post('kecamatan')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }
        $this
            ->add_css_theme(array(
                                  'easy-autocomplete.min.css'
                              ))
            ->add_js_theme(array('jquery.easy-autocomplete.min.js', 
                                 'ruass.js'
                             ))
                             
        // setup page header data
        ->set_title(lang('ruass title ruas_add'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'ruass'              => NULL
        );

        // load views
        $data['content'] = $this->load->view('ruass/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Edit existing ruass
     *
     * @param  int $id
     */
    function edit($id=NULL)
    {
        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $ruass = $this->ruass_model->get_ruas($id);

        // if empty results, return to list
        if ( ! $ruass)
        {
            redirect($this->_redirect_url);
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('no_ruas', lang('ruass input no_ruas'), 'required|trim|min_length[1]|max_length[30]');
        $this->form_validation->set_rules('nm_ruas', lang('ruass input nm_ruas'), 'required|trim|min_length[1]|max_length[32]');
        $this->form_validation->set_rules('tahun_data', lang('ruass input tahun_data'), 'trim|numeric');
        $this->form_validation->set_rules('propinsi', lang('ruass input propinsi'), 'required|trim');
        $this->form_validation->set_rules('kota_kab', lang('ruass input kota_kab'), 'required|trim');
        $this->form_validation->set_rules('kecamatan', lang('ruass input kecamatan'), 'required|trim');
        $this->form_validation->set_rules('desa_kel', lang('ruass input desa_kel'), 'required|trim');
        $this->form_validation->set_rules('panjang', lang('ruass input panjang'), 'required|trim|numeric');
        $this->form_validation->set_rules('lebar', lang('ruass input lebar'), 'required|trim|numeric');
        $this->form_validation->set_rules('fungsi', lang('ruass input fungsi'), 'required|trim');
        $this->form_validation->set_rules('kategori', lang('ruass input kategori'), 'required|trim');
        $this->form_validation->set_rules('status_jalan', lang('ruass input status_jalan'), 'required|trim');

        if ($this->form_validation->run() == TRUE)
        {
            // save the changes
            $saved = $this->ruass_model->edit_ruas($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('ruass msg edit_ruas_success'), $this->input->post('nm_ruas') . ", " . $this->input->post('kecamatan')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('ruass error edit_ruas_failed'), $this->input->post('nm_ruas') . ", " . $this->input->post('kecamatan')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }
        $this
            ->add_css_theme(array(
                                  'easy-autocomplete.min.css'
                              ))
            ->add_js_theme(array('jquery.easy-autocomplete.min.js', 
                                 'ruass.js'
                             ))
        // setup page header data
            ->set_title(lang('ruass title ruass_edit'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'ruass'              => $ruass,
            'id'           => $id
        );

        // load views
        $data['content'] = $this->load->view('ruass/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Delete a ruas
     *
     * @param  int $id
     */
    function delete($id=NULL)
    {
        // make sure we have a numeric id
        if ( ! is_null($id) OR ! is_numeric($id))
        {
            // get ruas details
            $ruas = $this->ruass_model->get_ruas($id);

            if ($ruas)
            {
                // soft-delete the ruas
                $delete = $this->ruass_model->delete_ruas($id);

                if ($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('ruass msg delete_ruas'), $ruas['nm_ruas'] . " " . $ruas['propinsi']." ". $ruas['kota_kab']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('ruass error delete_ruas'), $ruas['nm_ruas'] . " " . $ruas['propinsi']." ". $ruas['kota_kab']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('ruass error ruas_not_exist'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('ruass error ruas_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }

    function call_autocomplete()
    {
        $jenis = $this->input->post('jenis');
        $phrase = $this->input->post('phrase');
        $data = $this->autocomplete_model->get_ruass_data($jenis, $phrase);

        if ($data['results'])
        {
            echo json_encode($data['results']);
        }
        else
        {
            return FALSE;
        }
    }
   

    /**************************************************************************************
     * PRIVATE VALIDATION CALLBACK FUNCTIONS
     **************************************************************************************/


    /**
     * Make sure no_ruas is available
     *
     * @param  string $no_ruas
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_no_ruas($no_ruas, $current)
    {
        if (trim($no_ruas) != trim($current) && $this->ruass_model->no_ruas_exists($no_ruas))
        {
            $this->form_validation->set_message('_check_no_ruas', sprintf(lang('ruass error no_ruas_exists'), $no_ruas));
            return FALSE;
        }
        else
        {
            return $no_ruas;
        }
    }


    /**
     * Make sure email is available
     *
     * @param  string $email
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_email($email, $current)
    {
        if (trim($email) != trim($current) && $this->ruass_model->email_exists($email))
        {
            $this->form_validation->set_message('_check_email', sprintf(lang('ruass error email_exists'), $email));
            return FALSE;
        }
        else
        {
            return $email;
        }
    }

}

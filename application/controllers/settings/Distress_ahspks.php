<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Distress_ahspks extends Private_Controller {

    /**
     * @var string
     */
    private $_redirect_url;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // Ambil data session tahun
        $this->load->library('session');
        $this->pengguna = $this->session->userdata('logged_in');
        // load the language files
        $this->lang->load('distress_ahspks');
        $this->lang->load('admin');

        // load the ruass model
        $this->load->model('distress_ahspks_model');

        // load the roles model
       // $this->load->model('roles_model');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('settings/distress_ahspks'));

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * User list page
     */
    function index()
    {
        // get list
        $get_distresses        = $this->distress_ahspks_model->get_distresses();
        $get_ahspks            = $this->distress_ahspks_model->get_ahspks();
        $get_distresses_ahspks   = $this->distress_ahspks_model->get_distress_ahspks();

        // setup page header data
		$this
            ->add_css_theme(array(
                                'responsive.dataTables.min.css',
                                'datatables.min.css',
                                'select2/select2.min.css'
                              ))
			

            ->add_js_theme(array(
                                 
                                 'datatables.min.js',
                                 'dataTables.responsive.min.js',
                                 'distress_ahspks.js',
                                 'select2/select2.min.js'
                             ))
			->set_title(lang('distress_ahspks title list'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'          => THIS_URL,
            'distresses'        => $get_distresses['results'],
            'ahspks'            => $get_ahspks['results'],
            'distresses_ahspks' => $get_distresses_ahspks['results'],
            'tahun'             => $this->pengguna['tahun'],
            'total'             => $get_distresses['total']
        );
        //dtc($distress_ahspks['results']);
        // load views
        $data['content'] = $this->load->view('settings/distress_ahspks/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Add new ruas
     */
    function add()
    {
            // save the new ruas
            $saved = $this->distress_ahspks_model->add($this->input->post());

            if ($saved)
            {
                echo json_encode(array("status"=>"TRUE", "id"=>$saved['id']));
            }
            else
            {
                echo json_encode(array("status"=>"FALSE"));
            }

    }

    /**
     * get filtered HSP
     */
    function get_hsps()
    {
            // get hsp based on distress
            $get_ahspks_all = $this->distress_ahspks_model->get_ahspks();
            $get_ahspks = $this->distress_ahspks_model->get_distress_ahspks($this->input->post('id'));

            if ($get_ahspks)
            {
                echo json_encode(array("status"=>"TRUE", "ahspks"=>$get_ahspks['results'], "ahspks_all"=>$get_ahspks_all['results']));
            }
            else
            {
                echo json_encode(array("status"=>"FALSE"));
            }

    }

    /**
     * Delete a hsp
     *
     * @param  int $id
     */
    function delete()
    {
        // make sure we have a numeric id
        $id = $this->input->post('id',TRUE);
        if ( ! is_null($id) OR ! is_numeric($id))
        {
                // delete hsp
                $delete = $this->distress_ahspks_model->delete_distress_ahspks($id);

                if ($delete)
                {
                    echo json_encode(array("status"=>"TRUE"));
                }
                else
                {
                    echo json_encode(array("status"=>"FALSE"));
                }
        }
        else {
            echo json_encode(array("status"=>"Can not be processed"));
        }


        // return to list and display message
        //redirect($this->_redirect_url);
    }
}

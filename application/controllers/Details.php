<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Details extends Private_Controller {

    /**
     * @var string
     */
    private $_redirect_url;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->pengguna = $this->session->userdata('logged_in');

        // load the language files
        $this->lang->load('details');
        $this->lang->load('admin');
        $this->load->helper('format');

        // load the ruass model
        $this->load->model('details_model');
        $this->load->model('pakets_model');
        $this->load->model('ruass_model');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('details'));

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * User list page
     */
    function stationing($ruas=NULL)
    {
        // get list
        $details = $this->details_model->get_detail($ruas);
        $ruas_id = $this->pakets_model->get_ruas_by_paket($ruas);
        $keterangan = $this->ruass_model->get_ruas($ruas_id['ref_jalan_id']);

        // setup page header data
		$this
            ->add_css_theme(array(
                                  'leaflet.css',
                                  'map.css',
                                  'datatables.min.css',
                                  'responsive.dataTables.min.css'
                              ))
			

            ->add_js_theme(array('leaflet.js', 
                                 'leaflet-ant-path.js',
                                 'datatables.min.js',
                                 'dataTables.responsive.min.js',
                                 'stationing.js'
                             ))
			->set_title(lang('details title detail_stationing'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'ruas'       => $keterangan,
            'stations'   => $details['station'],
            'segments'   => $details['segment'],
            'paket'      => $ruas_id['paket_name'],
            'paket_id'   => $ruas,
        );
        //dtc($details['results']);
        // load views
        $data['content'] = $this->load->view('details/stationing', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function survey($ruas=NULL)
    {
        // get list
        $details = $this->details_model->get_detail($ruas);
        $ruas_id = $this->pakets_model->get_ruas_by_paket($ruas);
        $keterangan = $this->ruass_model->get_ruas($ruas_id['ref_jalan_id']);

        // setup page header data
        $this
            ->add_css_theme(array(
                                  'leaflet.css',
                                  'map.css',
                                  'datatables.min.css',
                                  'responsive.dataTables.min.css'
                              ))
            

            ->add_js_theme(array('leaflet.js', 
                                 'leaflet-ant-path.js',
                                 'datatables.min.js',
                                 'dataTables.responsive.min.js',
                                 'chart.bundle.min.js',
                                 'survey.js'
                             ))
            ->set_title(lang('details title detail_survey'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'ruas'       => $keterangan,
            'stations'   => $details['station'],
            'segments'   => $details['segment'],
            'kerusakans' => $details['kerusakan'],
            'kerusakans_grup' => $details['kerusakan_grup'],
            'paket'      => $ruas_id['paket_name'],
            'paket_id'   => $ruas,
        );
        dtc(json_encode($details['kerusakan_grup']));
        // load views
        $data['content'] = $this->load->view('details/survey', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

 function pci($ruas=NULL)
    {
        // get list
        $details = $this->details_model->get_detail($ruas);
        $ruas_id = $this->pakets_model->get_ruas_by_paket($ruas);
        $keterangan = $this->ruass_model->get_ruas($ruas_id['ref_jalan_id']);
        //$keterangan = $this->ruass_model->get_ruas($ket);

        // setup page header data
        $this
            ->add_css_theme(array(
                                  'datatables.min.css',
                                  'responsive.dataTables.min.css'
                              ))
            

            ->add_js_theme(array(
                                 'datatables.min.js',
                                 'dataTables.responsive.min.js',
                                 'chart.bundle.min.js',
                                 'pci.js'
                             ))
            ->set_title(lang('details title detail_pci'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'pcis'       => $details['pci'],
            'pci_rerata' => $details['pci_rerata'],
            'paket'      => $ruas_id['paket_name'],
            'paket_id'   => $ruas,
        );
        //dtc($details['pci_rerata']);
        // load views
        $data['content'] = $this->load->view('details/pci', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


 function rekomendasi($ruas=NULL)
    {
        // get list

        $details    = $this->details_model->get_detail($ruas);
        $ruas_id    = $this->pakets_model->get_ruas_by_paket($ruas);
        $keterangan = $this->ruass_model->get_ruas($ruas_id['ref_jalan_id']);
        
        foreach ($details['rekomendasi'] as $key=>$row) {
          $biaya = array();
          $biaya = $this->details_model->get_distress_ahspk($row['tipe']);
          $hsp2  = array();
          $hsp3  = array();
          
          if ($biaya != NULL){
              foreach ($biaya as $data) {
              $hsp2['item'] = $data['nama_ahspk'];
              array_push($hsp3, $hsp2);
              }
          }
          else {
                $hsp3 = 0;
          }

          $details['rekomendasi'][$key]['item'] = $hsp3;
        }

        // setup page header data
        $this
            ->add_css_theme(array(
                                  'datatables.min.css',
                                  'responsive.dataTables.min.css'
                              ))
            ->add_js_theme(array(
                                 'datatables.min.js',
                                 'dataTables.responsive.min.js',
                                 'rekomendasi.js'
                             ))
            ->set_title(lang('details title detail_rekomendasi'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'          => THIS_URL,
            'ruas'              => $keterangan,
            'rekomendasis'      => $details['rekomendasi'],
            'pci_rerata'        => $details['pci_rerata'],
            'paket'             => $ruas_id['paket_name'],
            'paket_id'          => $ruas
        );
     
        // load views
        $data['content'] = $this->load->view('details/rekomendasi', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


 function estimasi($ruas=NULL)
    {
        // get list
        $details    = $this->details_model->get_detail($ruas);
        $ruas_id    = $this->pakets_model->get_ruas_by_paket($ruas);
        $keterangan = $this->ruass_model->get_ruas($ruas_id['ref_jalan_id']);
        
        $this->load->library('hsp');
        $total_harga = 0;
        foreach ($details['estimasi'] as $key=>$row) {
          $biaya = array();
          $biaya = $this->details_model->get_distress_ahspk($row['tipe']);
          $hsp   = array();
          $hsp2  = array();
          $hsp3  = array();
          
          if ($biaya != NULL){
          foreach ($biaya as $data) {
          $hsp   = new Hsp($row['panjang'],$row['lebar'],$row['kedalaman'],$data['bj_ahspk'],$data['harsat_ahspk'],$data['satuan_ahspk'],$data['satuan_bj_ahspk']);
          $hsp = $hsp->kalkulasi();
          $hsp2['nama'] = $data['nama_ahspk'];
          $hsp2['harga'] = $data['harsat_ahspk'];
          $hsp2['unit'] = $data['satuan_ahspk'];
          $hsp2['bj'] = $data['bj_ahspk'];
          $hsp2['unit_bj'] = $data['satuan_bj_ahspk'];
          $hsp2['biaya'] = $hsp;
          $total_harga += $hsp;
          array_push($hsp3, $hsp2);
            }
            }

            else {
                $hsp3 = 0;
            }
            $details['estimasi'][$key]['hsp'] = $hsp3;
          
        }

        // setup page header data
        $this
            ->add_css_theme(array(
                                  'datatables.min.css',
                                  'responsive.dataTables.min.css'
                              ))
            

            ->add_js_theme(array(
                                 'datatables.min.js',
                                 'dataTables.responsive.min.js',
                                 'estimasi.js'
                             ))
            ->set_title(lang('details title detail_estimasi'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'          => THIS_URL,
            'ruas'              => $keterangan,
            'estimasis'         => $details['estimasi'],
            'pci_rerata'        => $details['pci_rerata'],
            'paket'             => $ruas_id['paket_name'],
            'total_harga'       => $total_harga,
            'paket_id'          => $ruas
        );
     
        // load views
        $data['content'] = $this->load->view('details/estimasi', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


 function rekapitulasi($ruas=NULL)
    {
        // get list
        $details    = $this->details_model->get_detail($ruas);
        $ruas_id    = $this->pakets_model->get_ruas_by_paket($ruas);
        $keterangan = $this->ruass_model->get_ruas($ruas_id['ref_jalan_id']);
        
        $this->load->library('hsp');
        $total_harga = 0;
        foreach ($details['rekapitulasi'] as $key=>$row) {
          $biaya = array();
          $biaya = $this->details_model->get_distress_ahspk($row['tipe']);
          $hsp   = 0;
          $hsp2  = 0;
          //$hsp3  = array();
          
          if ($biaya != NULL){
          foreach ($biaya as $data) {
          //instantiate object  
          $hsp   = new Hsp($row['panjang'],$row['lebar'],$row['kedalaman'],$data['bj_ahspk'],$data['harsat_ahspk'],$data['satuan_ahspk'],$data['satuan_bj_ahspk']);
          $hsp = $hsp->kalkulasi();
          //get biaya
          $hsp2 += $hsp;
          $total_harga += $hsp;
          //array_push($hsp3, $hsp2);
            }
            }

            else {
                $hsp3 = 0;
            }
            $details['rekapitulasi'][$key]['hsp'] = $hsp2;
            
        }

        //Produce Table
            $hapus = $this->details_model->delete_rekapitulasi($ruas,$this->pengguna['tahun']);
            //$buat = $this->details_model->create_table($ruas,$this->pengguna['tahun']);
            $tambah = $this->details_model->add_rekapitulasi($ruas,$this->pengguna['tahun'],$details['rekapitulasi']);
            $rekap = array();
            if ($tambah) {
                $rekap['segmen'] = $this->details_model->get_rekap_segmen($ruas,$this->pengguna['tahun']);
                $rekap['distress'] = $this->details_model->get_rekap_distress($ruas,$this->pengguna['tahun']);
                
            } else {
               $rekap['segmen'] = NULL;
               $rekap['distress'] = NULL;
            }

        // setup page header data
        $this
            ->add_css_theme(array(
                                  'datatables.min.css',
                                  'responsive.dataTables.min.css',
                                  'select2/select2.min.css'
                              ))
            

            ->add_js_theme(array(
                                 'select2/select2.min.js',
                                 'datatables.min.js',
                                 'dataTables.responsive.min.js',
                                 'chart.bundle.min.js',
                                 'numeral/numeral.min.js',
                                 'rekapitulasi.js'
                             ))
            ->set_title(lang('details title detail_rekapitulasi'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'          => THIS_URL,
            'ruas'              => $keterangan,
            'rekapitulasis'     => $details['rekapitulasi'],
            'pci_rerata'        => $details['pci_rerata'],
            'paket'             => $ruas_id['paket_name'],
            'total_harga'       => $total_harga,
            'rekaps'             => $rekap,
            'paket_id'          => $ruas
        );
     
        // load views
        $data['content'] = $this->load->view('details/rekapitulasi', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Add new ruas
     */
    function add()
    {
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('no_ruas', lang('ruass input no_ruas'), 'required|trim|min_length[5]|max_length[30]|callback__check_no_ruas[]');
        $this->form_validation->set_rules('nm_ruas', lang('ruass input nm_ruas'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('kecamatan', lang('ruass input kecamatan'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('email', lang('ruass input email'), 'required|trim|max_length[128]|valid_email|callback__check_email[]');
        $this->form_validation->set_rules('language', lang('ruass input language'), 'required|trim');
        $this->form_validation->set_rules('status', lang('ruass input status'), 'required|numeric');
        $this->form_validation->set_rules('is_admin', lang('ruass input is_admin'), 'required|numeric');
        $this->form_validation->set_rules('role', lang('ruass input role'), 'required|numeric');
        $this->form_validation->set_rules('password', lang('ruass input password'), 'required|trim|min_length[5]');
        $this->form_validation->set_rules('password_repeat', lang('ruass input password_repeat'), 'required|trim|matches[password]');

        if ($this->form_validation->run() == TRUE)
        {
            // save the new ruas
            $saved = $this->ruass_model->add_ruas($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('ruass msg add_ruas_success'), $this->input->post('nm_ruas') . " " . $this->input->post('kecamatan')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('ruass error add_ruas_failed'), $this->input->post('nm_ruas') . " " . $this->input->post('kecamatan')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this->set_title(lang('ruass title ruas_add'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'ruas'              => NULL
        );

        // load views
        $data['content'] = $this->load->view('admin/ruass/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Edit existing user
     *
     * @param  int $id
     */
    function edit($id=NULL)
    {
        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $user = $this->users_model->get_user($id);

        // if empty results, return to list
        if ( ! $user)
        {
            redirect($this->_redirect_url);
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('no_ruas', lang('users input no_ruas'), 'required|trim|min_length[5]|max_length[30]|callback__check_no_ruas[' . $user['no_ruas'] . ']');
        $this->form_validation->set_rules('nm_ruas', lang('users input nm_ruas'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('kecamatan', lang('users input kecamatan'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[128]|valid_email|callback__check_email[' . $user['email'] . ']');
        $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');
        $this->form_validation->set_rules('status', lang('users input status'), 'required|numeric');
        $this->form_validation->set_rules('is_admin', lang('users input is_admin'), 'required|numeric');
        $this->form_validation->set_rules('role', lang('users input role'), 'required|numeric');
        $this->form_validation->set_rules('password', lang('users input password'), 'min_length[5]|matches[password_repeat]');
        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'matches[password]');

        if ($this->form_validation->run() == TRUE)
        {
            // save the changes
            $saved = $this->users_model->edit_user($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('users msg edit_user_success'), $this->input->post('nm_ruas') . " " . $this->input->post('kecamatan')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('users error edit_user_failed'), $this->input->post('nm_ruas') . " " . $this->input->post('kecamatan')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this->set_title(lang('users title user_edit'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'user'              => $user,
            'roles'             => $this->roles_model->get_roles(),
            'user_id'           => $id,
            'password_required' => FALSE
        );

        // load views
        $data['content'] = $this->load->view('admin/users/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Delete a ruas
     *
     * @param  int $id
     */
    function delete($id=NULL)
    {
        // make sure we have a numeric id
        if ( ! is_null($id) OR ! is_numeric($id))
        {
            // get ruas details
            $ruas = $this->ruass_model->get_ruas($id);

            if ($ruas)
            {
                // soft-delete the ruas
                $delete = $this->ruass_model->delete_ruas($id);

                if ($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('ruass msg delete_ruas'), $ruas['nm_ruas'] . " " . $ruas['kecamatan']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('ruass error delete_ruas'), $ruas['nm_ruas'] . " " . $ruas['kecamatan']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('ruass error ruas_not_exist'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('ruass error ruas_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }


    /**
     * Export list to CSV
     */
    function export()
    {
        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('no_ruas'))
        {
            $filters['no_ruas'] = $this->input->get('no_ruas', TRUE);
        }

        if ($this->input->get('nm_ruas'))
        {
            $filters['nm_ruas'] = $this->input->get('nm_ruas', TRUE);
        }

        if ($this->input->get('kecamatan'))
        {
            $filters['kecamatan'] = $this->input->get('kecamatan', TRUE);
        }

        // get all users
        $users = $this->users_model->get_all(0, 0, $filters, $sort, $dir);

        if ($users['total'] > 0)
        {
            // manipulate the output array
            foreach ($users['results'] as $key=>$user)
            {
                unset($users['results'][$key]['password']);
                unset($users['results'][$key]['deleted']);

                if ($user['status'] == 0)
                {
                    $users['results'][$key]['status'] = lang('admin input inactive');
                }
                else
                {
                    $users['results'][$key]['status'] = lang('admin input active');
                }
            }

            // export the file
            array_to_csv($users['results'], "users");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }


    /**************************************************************************************
     * PRIVATE VALIDATION CALLBACK FUNCTIONS
     **************************************************************************************/


    /**
     * Make sure no_ruas is available
     *
     * @param  string $no_ruas
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_no_ruas($no_ruas, $current)
    {
        if (trim($no_ruas) != trim($current) && $this->users_model->no_ruas_exists($no_ruas))
        {
            $this->form_validation->set_message('_check_no_ruas', sprintf(lang('users error no_ruas_exists'), $no_ruas));
            return FALSE;
        }
        else
        {
            return $no_ruas;
        }
    }


    /**
     * Make sure email is available
     *
     * @param  string $email
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_email($email, $current)
    {
        if (trim($email) != trim($current) && $this->users_model->email_exists($email))
        {
            $this->form_validation->set_message('_check_email', sprintf(lang('users error email_exists'), $email));
            return FALSE;
        }
        else
        {
            return $email;
        }
    }

}

<?php	defined('BASEPATH') OR exit('Akses skrip secara langsung tidak diijinkan');
/**
 * File Bahasa Indonesia - Admin
 */

// Titles
$lang['admin title admin']                = "Halaman Admin";

// Buttons
$lang['admin button csv_export']          = "Ekspor CSV";
$lang['admin button dashboard']           = "Dashboard";
$lang['admin button delete']              = "Delete";
$lang['admin button add']                 = "Add";
$lang['admin button edit']                = "Ubah";
$lang['admin button messages']            = "Pesan";
$lang['admin button settings']            = "Setting";
$lang['admin button users']               = "User";
$lang['admin button users_add']           = "Tambah User Baru";
$lang['admin button users_list']          = "Daftar User";
$lang['admin button ruass']               = "Road List";
$lang['admin button roles']               = "Group";
$lang['admin button tahuns']              = "Year";
$lang['admin button show']                = "Lihat Data";

// Tooltips
$lang['admin tooltip csv_export']         = "Ekspor file CSV dari semua hasil dengan penerapan penyaringan.";
$lang['admin tooltip filter']             = "Perbarui hasil berdasarkan penyaringan anda.";
$lang['admin tooltip filter_reset']       = "Bersihkan semua penyaringan dan pengurutan anda.";

// Form Inputs
$lang['admin input active']               = "Aktif";
$lang['admin input inactive']             = "Non Aktif";
$lang['admin input items_per_page']       = "item/halaman";
$lang['admin input select']               = "pilih...";
$lang['admin input username']             = "Username";

// Table Columns
$lang['admin col actions']                = "Action";
$lang['admin col status']                 = "Status";

// Form Labels
$lang['admin label rows']                 = "%s data";

<?php	defined('BASEPATH') OR exit('Akses skrip secara langsung tidak diijinkan');
/**
 * File Bahasa Indonesia - Core
 */

// Buttons
$lang['core button admin']                  = "Admin";
$lang['core button ruass']                   = "Road Listing";
$lang['core button cancel']              	= "Cancel";
$lang['core button close']               	= "Close";
$lang['core button contact']               	= "Contact";
$lang['core button filter']              	= "Penyaringan";
$lang['core button home']                	= "Home";
$lang['core button login']               	= "Login";
$lang['core button logout']              	= "Logout";
$lang['core button profile']              	= "Profile Setting";
$lang['core button paket']              	= "Project List";
$lang['core button settings']              	= "Settings";
$lang['core button distress_ahspk']         = "Distress and Work Items Setting";
$lang['core button analisa']              	= "Analysis";
$lang['core button reset']               	= "Reset";
$lang['core button save']                	= "Save";
$lang['core button search']              	= "Fine";
$lang['core button toggle_nav']          	= "Toggle navigation";

//addition
$lang['core button infos']                  = "User Info";

// Text
$lang['core text no']                    	= "Tidak";
$lang['core text yes']                   	= "Ya";
$lang['core text page_rendered']            = "Page proccessed in <strong>{elapsed_time}</strong> seconds";

// Emails
$lang['core email start']                	= "<!DOCTYPE html><html><head><style>
                                                    body { background: #fff; color: #333; }
                                               </style></head><body>";
$lang['core email end']                  	= "</body></html>";

// Additional date_lang
$lang['UM75']	                         	= "(UTC +7:00) Asia/Jakarta Time";

// Errors
$lang['core error no_results']              = "No records found!";
$lang['core error session_language']        = "Ada masalah pengaturan bahasa!";

<?php	defined('BASEPATH') OR exit('Akses skrip secara langsung tidak diijinkan');
/**
 * File Bahasa Indonesia - pakets
 */

// Titles
$lang['pakets title paket_add']                 = "Add Project";
$lang['pakets title paket_delete']              = "Konfirmasi Hapus Paket Pekerjaan";
$lang['pakets title paket_edit']                = "Ubah Paket Pekerjaan";
$lang['pakets title paket_list']                = "Project List";

// Buttons
$lang['pakets button add_new_paket']            = "Add Project";

// Tooltips
$lang['pakets tooltip add_new_paket']           = "Add Project";

// Table Columns
$lang['pakets col no']                         = "No";
$lang['pakets col paket_name']                 = "Project Name";
$lang['pakets col jenis_pemel']                = "Project Type";
$lang['pakets col nm_ruas']                    = "Pavement Name";
$lang['pakets col panjang']                    = "Length";
$lang['pakets col lebar']                      = "Width";
$lang['pakets col fungsi']                     = "Function";
$lang['pakets col propinsi']                   = "Province";
$lang['pakets col kota_kab']                   = "City";
$lang['pakets col kecamatan']                  = "District";

// Form Inputs
$lang['pakets input nm_paket']                  = "Nama paket";
$lang['pakets input no_paket']                  = "Nomor paket";
$lang['pakets input tahun']                    = "Tahun Pendataan";
$lang['pakets input status_paket']              = "Status paket Pekerjaan";
$lang['pakets input fungsi_paket']              = "Fungsi paket Pekerjaan";
$lang['pakets input kategori_paket']            = "Kategori paket Pekerjaan";
$lang['pakets input mendukung']                = "Mendukung sektor";
$lang['pakets input propinsi']                 = "Propinsi";
$lang['pakets input kota_kab']                 = "Kota/Kabupaten";
$lang['pakets input kecamatan']                = "Kecamatan";
$lang['pakets input panjang']                  = "Panjang paket";
$lang['pakets input lebar']                    = "Lebar paket";
$lang['pakets input jalur']                    = "Jumlah Jalur";
$lang['pakets input lajur']                    = "Jumlah Lajur";
$lang['pakets input lhrt']                     = "LHRt";





// Messages
$lang['pakets msg add_paket_success']           = "%s berhasil ditambahkan!";
$lang['pakets msg delete_confirm']             = "Apakah anda yakin ingin menghapus <strong>%s</strong>? Ini tidak bisa dibatalkan.";
$lang['pakets msg delete_paket']                = "Anda telah berhasil menghapus <strong>%s</strong>!";
$lang['pakets msg edit_paket_success']          = "%s berhasil diperbarui!";

// Errors
$lang['pakets error add_paket_failed']          = "%s tidak bisa ditambahkan!";
$lang['pakets error delete_paket']              = "<strong>%s</strong> tidak bisa dihapus!";
$lang['pakets error edit_profile_failed']      = "Profil anda tidak bisa diubah!";
$lang['pakets error edit_paket_failed']         = "%s tidak bisa diubah!";
$lang['pakets error paket_id_required']         = "User ID harus berisi angka!";
$lang['pakets error paket_not_exist']           = "User tersebut tidak ada!";
$lang['pakets error username_exists']          = "Username <strong>%s</strong> sudah ada!";

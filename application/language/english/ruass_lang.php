<?php	defined('BASEPATH') OR exit('Akses skrip secara langsung tidak diijinkan');
/**
 * File Bahasa Indonesia - Ruass
 */

// Titles
$lang['ruass title ruas_add']                 = "Tambah Ruas Jalan";
$lang['ruass title ruas_delete']              = "Konfirmasi Hapus Ruas Jalan";
$lang['ruass title ruas_edit']                = "Ubah Ruas Jalan";
$lang['ruass title ruas_list']                = "Daftar Ruas Jalan";

// Buttons
$lang['ruass button add_new_ruas']            = "Tambah Ruas Jalan Baru";

// Tooltips
$lang['ruass tooltip add_new_ruas']           = "Buat ruas baru.";

// Table Columns
$lang['ruass col ruas_id']                    = "No.";
$lang['ruass col no']                         = "No";
$lang['ruass col no_ruas']                    = "Nomor Register Ruas";
$lang['ruass col nm_ruas']                    = "Nama Ruas Jalan";
$lang['ruass col fungsi']                     = "Fungsi";
$lang['ruass col propinsi']                   = "Propinsi";
$lang['ruass col kecamatan']                  = "Kecamatan";
$lang['ruass col kota_kab']                   = "Kota/Kaupaten";
$lang['ruass col panjang']                    = "Panjang(m)";
$lang['ruass col lebar']                      = "Lebar(m)";

// Form Inputs
$lang['ruass input nm_ruas']                  = "Nama Ruas Jalan";
$lang['ruass input no_ruas']                  = "Nomor Register Ruas";
$lang['ruass input tahun_data']               = "Tahun Pendataan";
$lang['ruass input status_jalan']             = "Status (Jalan Lingkungan/Jalan Kota/Jalan Propinsi/Jalan Nasional)";
$lang['ruass input fungsi']                   = "Fungsi (Kolektor/Arteri)";
$lang['ruass input kategori']                 = "Kategori (Strategis/Non Strategis)";
$lang['ruass input mendukung']                = "Mendukung Sektor";
$lang['ruass input propinsi']                 = "Propinsi";
$lang['ruass input kota_kab']                 = "Kota/Kabupaten";
$lang['ruass input kecamatan']                = "Kecamatan";
$lang['ruass input desa_kel']                 = "Kelurahan/Desa";
$lang['ruass input panjang']                  = "Panjang Ruas(m)";
$lang['ruass input lebar']                    = "Lebar Ruas(m)";
$lang['ruass input jalur']                    = "Jumlah Jalur";
$lang['ruass input lajur']                    = "Jumlah Lajur";
$lang['ruass input lhrt']                     = "LHRt (LHR Tahunan)";





// Messages
$lang['ruass msg add_ruas_success']           = "%s berhasil ditambahkan!";
$lang['ruass msg delete_confirm']             = "Apakah anda yakin ingin menghapus <strong>%s</strong>? Ini tidak bisa dibatalkan.";
$lang['ruass msg delete_ruas']                = "Anda telah berhasil menghapus <strong>%s</strong>!";
$lang['ruass msg edit_ruas_success']          = "%s berhasil diperbarui!";

// Errors
$lang['ruass error add_ruas_failed']          = "%s tidak bisa ditambahkan!";
$lang['ruass error delete_ruas']              = "<strong>%s</strong> tidak bisa dihapus!";
$lang['ruass error edit_profile_failed']      = "Profil anda tidak bisa diubah!";
$lang['ruass error edit_ruas_failed']         = "%s tidak bisa diubah!";
$lang['ruass error ruas_id_required']         = "User ID harus berisi angka!";
$lang['ruass error ruas_not_exist']           = "User tersebut tidak ada!";
$lang['ruass error username_exists']          = "Username <strong>%s</strong> sudah ada!";

<?php	defined('BASEPATH') OR exit('Akses skrip secara langsung tidak diijinkan');
/**
 * File Bahasa Indonesia - details
 */

// Titles
$lang['details title detail_add']                 = "Tambah detail Pekerjaan";
$lang['details title detail_delete']              = "Konfirmasi Hapus detail Pekerjaan";
$lang['details title detail_edit']                = "Ubah Detail Pekerjaan";
$lang['details title detail_stationing']          = "Stationing Detail";
$lang['details title detail_survey']                = "Distress Survey Detail";
$lang['details title detail_pci']                = "PCI Value Detail";
$lang['details title detail_rekomendasi']                = "Treatment Recommendation Detail";
$lang['details title detail_estimasi']                = "Cost Estimation Detail";
$lang['details title detail_rekapitulasi']                = "Summary Detail";

// Buttons
$lang['details button add_new_detail']            = "Tambah detail Pekerjaan";

// Tooltips
$lang['details tooltip add_new_detail']           = "Buat detail baru.";

// Table Columns
$lang['details col no']                         = "No";
$lang['details col detail_name']                 = "Nama detail";
$lang['details col jenis_pemel']                = "Jenis Pekerjaan";
$lang['details col nm_ruas']                    = "Nama Ruas Jalan";
$lang['details col panjang']                    = "Panjang";
$lang['details col lebar']                      = "Lebar";
$lang['details col fungsi']                     = "Fungsi";
$lang['details col propinsi']                   = "Propinsi";
$lang['details col kota_kab']                   = "Kota/Kab";
$lang['details col kecamatan']                  = "Kecamatan";
$lang['stations col no_station']                = "Station Number";
$lang['stations col lat']                = "Latitude";
$lang['stations col lon']                = "Longitude";
$lang['stations col segmen']                = "Segment";
$lang['stations col latlon']                = "Coordinate";
$lang['stations col tipe']                = "Distress Code";
$lang['stations col namker']                = "Distress Name";
$lang['stations col parah']                = "Distress Level";
$lang['stations col panjang']                = "Length";
$lang['stations col lebar']                = "Width";
$lang['stations col luas']                = "Area";
$lang['stations col kedalaman']                = "Depth";
$lang['stations col volume']                = "Volume";
$lang['stations col foto']                = "Picture";
$lang['pcis col segmen']                = "Segment";
$lang['pcis col pci']                = "PCI Value";
$lang['rekomendasis col segmen']                = "Segment";
$lang['rekomendasis col pci']                = "PCI Value";
$lang['rekomendasis col treatment']                = "Treatment Recommendation";
$lang['rekomendasis col distress']                = "Distress Type";
$lang['rekomendasis col definition']                = "Distress Description";
$lang['rekomendasis col ahspk']                = "Work Items";
$lang['estimasis col segmen']                = "Segment";
$lang['estimasis col pci']                = "PCI Value";
$lang['estimasis col treatment']                = "Treatment Recommendation";
$lang['estimasis col distress']                = "Distress Type";
$lang['estimasis col ahspk']                = "Work Items";
$lang['estimasis col harga_ahspk']                = "Cost Estimation";
$lang['rekapitulasis col segmen']                = "Segment";
$lang['rekapitulasis col pci']                = "PCI Value";
$lang['rekapitulasis col treatment']                = "Treatment Recommendation";
$lang['rekapitulasis col distress']                = "Distress Type";
$lang['rekapitulasis col jml_distress']                = "Quantity";
$lang['rekapitulasis col ahspk']                = "Work Items";
$lang['rekapitulasis col harga_ahspk']                = "Cost Estimation (Rp.)";
// Form Inputs
$lang['details input nm_detail']                  = "Nama detail";
$lang['details input no_detail']                  = "Nomor detail";
$lang['details input tahun']                    = "Tahun Pendataan";
$lang['details input status_detail']              = "Status detail Pekerjaan";
$lang['details input fungsi_detail']              = "Fungsi detail Pekerjaan";
$lang['details input kategori_detail']            = "Kategori detail Pekerjaan";
$lang['details input mendukung']                = "Mendukung sektor";
$lang['details input propinsi']                 = "Propinsi";
$lang['details input kota_kab']                 = "Kota/Kabupaten";
$lang['details input kecamatan']                = "Kecamatan";
$lang['details input panjang']                  = "Panjang detail";
$lang['details input lebar']                    = "Lebar detail";
$lang['details input jalur']                    = "Jumlah Jalur";
$lang['details input lajur']                    = "Jumlah Lajur";
$lang['details input lhrt']                     = "LHRt";





// Messages
$lang['details msg add_detail_success']           = "%s berhasil ditambahkan!";
$lang['details msg delete_confirm']             = "Apakah anda yakin ingin menghapus <strong>%s</strong>? Ini tidak bisa dibatalkan.";
$lang['details msg delete_detail']                = "Anda telah berhasil menghapus <strong>%s</strong>!";
$lang['details msg edit_detail_success']          = "%s berhasil diperbarui!";

// Errors
$lang['details error add_detail_failed']          = "%s tidak bisa ditambahkan!";
$lang['details error delete_detail']              = "<strong>%s</strong> tidak bisa dihapus!";
$lang['details error edit_profile_failed']      = "Profil anda tidak bisa diubah!";
$lang['details error edit_detail_failed']         = "%s tidak bisa diubah!";
$lang['details error detail_id_required']         = "User ID harus berisi angka!";
$lang['details error detail_not_exist']           = "User tersebut tidak ada!";
$lang['details error username_exists']          = "Username <strong>%s</strong> sudah ada!";

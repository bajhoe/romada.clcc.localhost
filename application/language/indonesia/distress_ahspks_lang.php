<?php	defined('BASEPATH') OR exit('Akses skrip secara langsung tidak diijinkan');
/**
 * File Bahasa Indonesia - pakets
 */

// Titles
$lang['distress_ahspks title paket_add']                 = "Tambah Item HSP";
$lang['distress_ahspks title paket_delete']              = "Konfirmasi Hapus Item HSP";
$lang['distress_ahspks title paket_edit']                = "Ubah Paket Pekerjaan";
$lang['distress_ahspks title list']                = "Daftar Jenis Kerusakan dan Item HSP";

// Buttons
$lang['distress_ahspks button add_new_paket']            = "Tambah Item HSP";

// Tooltips
$lang['distress_ahspks tooltip add_new_paket']           = "Tambah Item HSP";

// Table Columns
$lang['distress_ahspks col no']                         = "No";
$lang['distress_ahspks col distress_name']              = "Jenis Kerusakan";
$lang['distress_ahspks col distress_level']             = "Tingkat Jenis Kerusakan";
$lang['distress_ahspks col distress_code']              = "Kode Jenis Kerusakan";
$lang['distress_ahspks col ahspk']                      = "Item HSP";
$lang['distress_ahspks col panjang']                    = "Panjang";
$lang['distress_ahspks col lebar']                      = "Lebar";
$lang['distress_ahspks col definition']                 = "Indikator Kerusakan";
$lang['distress_ahspks col treatment']                  = "Pilihan Penanganan";
$lang['distress_ahspks col fungsi']                     = "Fungsi";
$lang['distress_ahspks col propinsi']                   = "Propinsi";
$lang['distress_ahspks col kota_kab']                   = "Kota/Kab";
$lang['distress_ahspks col kecamatan']                  = "Kecamatan";

// Form Inputs
$lang['distress_ahspks input nm_paket']                  = "Nama paket";
$lang['distress_ahspks input no_paket']                  = "Nomor paket";
$lang['distress_ahspks input tahun']                    = "Tahun Pendataan";
$lang['distress_ahspks input status_paket']              = "Status paket Pekerjaan";
$lang['distress_ahspks input fungsi_paket']              = "Fungsi paket Pekerjaan";
$lang['distress_ahspks input kategori_paket']            = "Kategori paket Pekerjaan";
$lang['distress_ahspks input mendukung']                = "Mendukung sektor";
$lang['distress_ahspks input propinsi']                 = "Propinsi";
$lang['distress_ahspks input kota_kab']                 = "Kota/Kabupaten";
$lang['distress_ahspks input kecamatan']                = "Kecamatan";
$lang['distress_ahspks input panjang']                  = "Panjang paket";
$lang['distress_ahspks input lebar']                    = "Lebar paket";
$lang['distress_ahspks input jalur']                    = "Jumlah Jalur";
$lang['distress_ahspks input lajur']                    = "Jumlah Lajur";
$lang['distress_ahspks input lhrt']                     = "LHRt";





// Messages
$lang['distress_ahspks msg add_paket_success']           = "%s berhasil ditambahkan!";
$lang['distress_ahspks msg delete_confirm']             = "Apakah anda yakin ingin menghapus Item <strong>%s</strong>? Ini tidak bisa dibatalkan.";
$lang['distress_ahspks msg delete_paket']                = "Anda telah berhasil menghapus <strong>%s</strong>!";
$lang['distress_ahspks msg edit_paket_success']          = "%s berhasil diperbarui!";

// Errors
$lang['distress_ahspks error add_paket_failed']          = "%s tidak bisa ditambahkan!";
$lang['distress_ahspks error delete_paket']              = "<strong>%s</strong> tidak bisa dihapus!";
$lang['distress_ahspks error edit_profile_failed']      = "Profil anda tidak bisa diubah!";
$lang['distress_ahspks error edit_paket_failed']         = "%s tidak bisa diubah!";
$lang['distress_ahspks error paket_id_required']         = "User ID harus berisi angka!";
$lang['distress_ahspks error paket_not_exist']           = "User tersebut tidak ada!";
$lang['distress_ahspks error username_exists']          = "Username <strong>%s</strong> sudah ada!";

<?php	defined('BASEPATH') OR exit('Akses skrip secara langsung tidak diijinkan');
/**
 * File Bahasa Indonesia - roles
 */

// Titles
$lang['roles title role_add']                 = "Tambah Grup";
$lang['roles title role_delete']              = "Konfirmasi Hapus Grup";
$lang['roles title role_edit']                = "Ubah Grup";
$lang['roles title role_list']                = "Daftar Grup";

// Buttons
$lang['roles button add_new_role']            = "Tambah Grup Baru";
$lang['roles button register']                = "Buat Grup";

// Tooltips
$lang['roles tooltip add_new_role']           = "Buat Grup Baru.";

// Table Columns
$lang['roles col role_name']                  = "Nama Grup";
$lang['roles col role_id']                    = "ID";
$lang['roles col status']                     = "Status";


// Form Inputs
$lang['roles input role_name']                = "Nama Grup";
$lang['roles input role']                     = "Grup";
$lang['roles input tahap']                    = "Mempunyai hak untuk menggunakan Tahap:";
$lang['roles input status']                   = "Status";
$lang['roles input destination']              = "Mempunyai hak untuk menggunakan Tujuan:";

// Help
$lang['roles help passwords']                 = "Masukkan password hanya jika anda ingin merubahnya.";

// Messages
$lang['roles msg add_role_success']           = "%s berhasil ditambahkan!";
$lang['roles msg delete_confirm']             = "Apakah anda yakin ingin menghapus <strong>%s</strong>? Ini tidak bisa dibatalkan.";
$lang['roles msg delete_role']                = "Anda telah berhasil menghapus <strong>%s</strong>!";

$lang['roles msg edit_role_success']          = "%s berhasil diperbarui!";


// Errors
$lang['roles error add_role_failed']          = "%s tidak bisa ditambahkan!";
$lang['roles error delete_role']              = "<strong>%s</strong> tidak bisa dihapus!";
$lang['roles error edit_role_failed']         = "%s tidak bisa diubah!";
$lang['roles error role_id_required']         = "Grup ID harus berisi angka!";
$lang['roles error role_not_exist']           = "Grup tersebut tidak ada!";
$lang['roles error role_exists']               = "Grup tersebut sudah ada!";
$lang['roles error rolename_exists']          = "Nama Grup <strong>%s</strong> sudah ada!";


<?php	defined('BASEPATH') OR exit('Akses skrip secara langsung tidak diijinkan');
/**
 * File Bahasa Indonesia - Tahuns
 */

// Titles
$lang['tahuns title forgot']                   = "Lupa Password";
$lang['tahuns title login']                    = "Login";
$lang['tahuns title profile']                  = "Profil";
$lang['tahuns title register']                 = "Masuk";
$lang['tahuns title tahun_add']             = "Tambah Tahun";
$lang['tahuns title tahun_delete']          = "Konfirmasi Hapus Tahun";
$lang['tahuns title tahun_edit']            = "Ubah Tahun";
$lang['tahuns title tahun_list']            = "Daftar Tahun";

// Buttons
$lang['tahuns button add_new_tahun']        = "Tambah Tahun Baru";
$lang['tahuns button register']                = "Buat Akun";
$lang['tahuns button reset_password']          = "Reset Password";
$lang['tahuns button login_try_again']         = "Coba lagi";

// Tooltips
$lang['tahuns tooltip add_new_tahun']       = "Buat Tahun baru.";

// Links
$lang['tahuns link forgot_password']           = "Lupa password anda?";
$lang['tahuns link register_account']          = "Daftarkan akun.";

// Table Columns
$lang['tahuns col first_name']                 = "Nama Depan";
$lang['tahuns col is_admin']                   = "Admin";
$lang['tahuns col last_name']                  = "Nama Belakang";
$lang['tahuns col tahuns_id']                = "ID";
$lang['tahuns col tahuns_name']              = "Nama";
$lang['tahuns col role']                       = "Peran/Role";
$lang['tahuns col tahun']                   = "Tahun";

// Form Inputs
$lang['tahuns input email']                    = "Email";
$lang['tahuns input first_name']               = "Nama Depan";
$lang['tahuns input is_admin']                 = "Apakah Admin?";
$lang['tahuns input language']                 = "Bahasa";
$lang['tahuns input tahun']                 = "Nama Tahun";
$lang['tahuns input last_name']                = "Nama Belakang";
$lang['tahuns input no_telp']                  = "Nomor HP/WA";
$lang['tahuns input role']                     = "Peran/Role";
$lang['tahuns input password']                 = "Password";
$lang['tahuns input password_repeat']          = "Ulangi Password";
$lang['tahuns input status']                   = "Status";
$lang['tahuns input tahunname']                 = "tahunname";
$lang['tahuns input tahunname_email']           = "tahunname atau Email";

// Help
$lang['tahuns help passwords']                 = "Masukkan password hanya jika anda ingin merubahnya.";

// Messages
$lang['tahuns msg add_tahun_success']           = "%s berhasil ditambahkan!";
$lang['tahuns msg delete_confirm']             = "Apakah anda yakin ingin menghapus <strong>%s</strong>? Ini tidak bisa dibatalkan.";
$lang['tahuns msg delete_tahun']                = "Anda telah berhasil menghapus <strong>%s</strong>!";
$lang['tahuns msg edit_profile_success']       = "Profil anda berhasil diperbarui!";
$lang['tahuns msg edit_tahun_success']          = "%s berhasil diperbarui!";
$lang['tahuns msg register_success']           = "Terima kasih sudah mendaftar, %s! Periksa email anda untuk pesan konfirmasi. Sekali
                                                 akun anda diverifikasi, anda akan dapat masuk dengan informasi rahasia yang anda buat.";
$lang['tahuns msg password_reset_success']     = "Password anda telah direset, %s! Silakan periksa email anda untuk password sementara anda yang baru.";
$lang['tahuns msg validate_success']           = "Akun anda telah diverifikasi. Anda sekarang bisa login ke akun anda.";
$lang['tahuns msg email_new_account']          = "<p>Terima kasih sudah membuat akun di %s. Klik ltautan di bawah ini untuk melakukan validasi
                                                 alamat email anda dan mengaktifkan akun anda.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['tahuns msg email_new_account_title']    = "Akun Baru untuk %s";
$lang['tahuns msg email_password_reset']       = "<p>Password anda di %s telah direset. Klik tautan di bawah ini untuk login dengan
                                                 menggunakan password baru anda:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a>
                                                 Sekali anda login, pastikan untuk merubah password anda dengan yang dapat anda ingat.</p>";
$lang['tahuns msg email_password_reset_title'] = "Reset Password untuk %s";

// Errors
$lang['tahuns error add_tahun_failed']          = "%s tidak bisa ditambahkan!";
$lang['tahuns error delete_tahun']              = "<strong>%s</strong> tidak bisa dihapus!";
$lang['tahuns error edit_profile_failed']      = "Profil anda tidak bisa diubah!";
$lang['tahuns error edit_tahun_failed']         = "%s tidak bisa diubah!";
$lang['tahuns error email_exists']             = "Email <strong>%s</strong> sudah ada!";
$lang['tahuns error email_not_exists']         = "Email tidak ada!";
$lang['tahuns error invalid_login']            = "tahunname atau password tidak benar";
$lang['tahuns error register_failed']          = "Akun anda tidak dapat dibuat saat ini. Silakan coba lagi.";
$lang['tahuns error tahun_id_required']         = "Tahun ID harus berisi angka!";
$lang['tahuns error tahun_not_exist']           = "Tahun tersebut tidak ada!";
$lang['tahuns error tahun_exists']          = "Tahun <strong>%s</strong> sudah ada!";
$lang['tahuns error validate_failed']          = "Terdapat masalah dalam upaya validasi akun anda. Silakan coba lagi.";
$lang['tahuns error too_many_login_attempts']  = "Anda sudah membuat terlalu banyak upaya untuk log in terlalu cepat. Silahkan tunggu %s detik dan coba lagi.";

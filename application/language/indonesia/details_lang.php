<?php	defined('BASEPATH') OR exit('Akses skrip secara langsung tidak diijinkan');
/**
 * File Bahasa Indonesia - details
 */

// Titles
$lang['details title detail_add']                 = "Tambah detail Pekerjaan";
$lang['details title detail_delete']              = "Konfirmasi Hapus detail Pekerjaan";
$lang['details title detail_edit']                = "Ubah Detail Pekerjaan";
$lang['details title detail_stationing']          = "Detail Stationing Paket";
$lang['details title detail_survey']                = "Detail Survey Kerusakan";
$lang['details title detail_pci']                = "Detail Nilai PCI";
$lang['details title detail_rekomendasi']                = "Detail Rekomendasi Pemeliharaan";
$lang['details title detail_estimasi']                = "Detail Estimasi Biaya";
$lang['details title detail_rekapitulasi']                = "Detail Rekapitulasi";

// Buttons
$lang['details button add_new_detail']            = "Tambah detail Pekerjaan";

// Tooltips
$lang['details tooltip add_new_detail']           = "Buat detail baru.";

// Table Columns
$lang['details col no']                         = "No";
$lang['details col detail_name']                 = "Nama detail";
$lang['details col jenis_pemel']                = "Jenis Pekerjaan";
$lang['details col nm_ruas']                    = "Nama Ruas Jalan";
$lang['details col panjang']                    = "Panjang";
$lang['details col lebar']                      = "Lebar";
$lang['details col fungsi']                     = "Fungsi";
$lang['details col propinsi']                   = "Propinsi";
$lang['details col kota_kab']                   = "Kota/Kab";
$lang['details col kecamatan']                  = "Kecamatan";
$lang['stations col no_station']                = "Nomor STA";
$lang['stations col lat']                = "Latitude";
$lang['stations col lon']                = "Longitude";
$lang['stations col segmen']                = "Segmen";
$lang['stations col latlon']                = "Koordinat";
$lang['stations col tipe']                = "Kode Kerusakan";
$lang['stations col namker']                = "Nama Kerusakan";
$lang['stations col parah']                = "Tingkat Kerusakan";
$lang['stations col panjang']                = "Panjang";
$lang['stations col lebar']                = "Lebar";
$lang['stations col luas']                = "Luas";
$lang['stations col kedalaman']                = "Kedalaman";
$lang['stations col volume']                = "Volume";
$lang['stations col foto']                = "Foto";
$lang['pcis col segmen']                = "Segmen";
$lang['pcis col pci']                = "Nilai PCI";
$lang['rekomendasis col segmen']                = "Segmen";
$lang['rekomendasis col pci']                = "Nilai PCI";
$lang['rekomendasis col treatment']                = "Rekomendasi Pemeliharaan";
$lang['rekomendasis col distress']                = "Tipe Kerusakan";
$lang['rekomendasis col definition']                = "Definisi kerusakan";
$lang['rekomendasis col ahspk']                = "Item Pekerjaan";
$lang['estimasis col segmen']                = "Segmen";
$lang['estimasis col pci']                = "Nilai PCI";
$lang['estimasis col treatment']                = "Rekomendasi";
$lang['estimasis col distress']                = "Tipe Kerusakan";
$lang['estimasis col ahspk']                = "Item Pekerjaan";
$lang['estimasis col harga_ahspk']                = "Estimasi Biaya Item Pekerjaan";
$lang['rekapitulasis col segmen']                = "Segmen";
$lang['rekapitulasis col pci']                = "Nilai PCI";
$lang['rekapitulasis col treatment']                = "Rekomendasi";
$lang['rekapitulasis col distress']                = "Tipe Kerusakan";
$lang['rekapitulasis col ahspk']                = "Item Pekerjaan";
$lang['rekapitulasis col harga_ahspk']                = "Estimasi Biaya(Rp.)";
// Form Inputs
$lang['details input nm_detail']                  = "Nama detail";
$lang['details input no_detail']                  = "Nomor detail";
$lang['details input tahun']                    = "Tahun Pendataan";
$lang['details input status_detail']              = "Status detail Pekerjaan";
$lang['details input fungsi_detail']              = "Fungsi detail Pekerjaan";
$lang['details input kategori_detail']            = "Kategori detail Pekerjaan";
$lang['details input mendukung']                = "Mendukung sektor";
$lang['details input propinsi']                 = "Propinsi";
$lang['details input kota_kab']                 = "Kota/Kabupaten";
$lang['details input kecamatan']                = "Kecamatan";
$lang['details input panjang']                  = "Panjang detail";
$lang['details input lebar']                    = "Lebar detail";
$lang['details input jalur']                    = "Jumlah Jalur";
$lang['details input lajur']                    = "Jumlah Lajur";
$lang['details input lhrt']                     = "LHRt";





// Messages
$lang['details msg add_detail_success']           = "%s berhasil ditambahkan!";
$lang['details msg delete_confirm']             = "Apakah anda yakin ingin menghapus <strong>%s</strong>? Ini tidak bisa dibatalkan.";
$lang['details msg delete_detail']                = "Anda telah berhasil menghapus <strong>%s</strong>!";
$lang['details msg edit_detail_success']          = "%s berhasil diperbarui!";

// Errors
$lang['details error add_detail_failed']          = "%s tidak bisa ditambahkan!";
$lang['details error delete_detail']              = "<strong>%s</strong> tidak bisa dihapus!";
$lang['details error edit_profile_failed']      = "Profil anda tidak bisa diubah!";
$lang['details error edit_detail_failed']         = "%s tidak bisa diubah!";
$lang['details error detail_id_required']         = "User ID harus berisi angka!";
$lang['details error detail_not_exist']           = "User tersebut tidak ada!";
$lang['details error username_exists']          = "Username <strong>%s</strong> sudah ada!";

<?php	defined('BASEPATH') OR exit('Akses skrip secara langsung tidak diijinkan');
/**
 * File Bahasa Indonesia - Core
 */

// Buttons
$lang['core button admin']                  = "Admin";
$lang['core button cancel']              	= "Batal";
$lang['core button close']               	= "Tutup";
$lang['core button contact']               	= "Kontak";
$lang['core button filter']              	= "Penyaringan";
$lang['core button home']                	= "Home";
$lang['core button login']               	= "Masuk";
$lang['core button logout']              	= "Keluar";
$lang['core button profile']              	= "Atur Profil";
$lang['core button paket']              	= "Daftar Paket Pekerjaan";
$lang['core button settings']              	= "Pengaturan";
$lang['core button distress_ahspk']         = "Tipe Kerusakan & AHSPK";
$lang['core button analisa']              	= "Analisa";
$lang['core button reset']               	= "Reset";
$lang['core button save']                	= "Simpan";
$lang['core button search']              	= "Cari";
$lang['core button toggle_nav']          	= "Toggle navigation";

//addition
$lang['core button infos']                  = "Info Pengguna";

// Text
$lang['core text no']                    	= "Tidak";
$lang['core text yes']                   	= "Ya";
$lang['core text page_rendered']            = "Halaman diberikan di <strong>{elapsed_time}</strong> detik";

// Emails
$lang['core email start']                	= "<!DOCTYPE html><html><head><style>
                                                    body { background: #fff; color: #333; }
                                               </style></head><body>";
$lang['core email end']                  	= "</body></html>";

// Additional date_lang
$lang['UM75']	                         	= "(UTC +7:00) Asia/Jakarta Time";

// Errors
$lang['core error no_results']              = "Tidak ada hasil yang ditemukan!";
$lang['core error session_language']        = "Ada masalah pengaturan bahasa!";

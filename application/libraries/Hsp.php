<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Hsp {

	protected $CI;
	protected $panjang;
	protected $lebar;
	protected $tebal;
	protected $hsp;
	protected $bj;
	protected $satuan;
	protected $satuan_bj;
	protected $hasil;


    /**
     * Constructor
     */
    public function __construct($p = null, $l = null, $t = null, $bj = null, $hsp = null, $satuan = null, $satuan_bj = null)
    {
        $this->CI      =& get_instance();
        $this->panjang = $p;
        $this->lebar   = $l;
        $this->tebal   = $t;
        $this->hsp     = $hsp;
        $this->bj      = $bj;
        $this->satuan  = $satuan;
        $this->satuan_bj  = $satuan_bj;
        $this->hasil   = null;

        // load file helper
        //$this->CI->load->helper('file');
    }

    public function kalkulasi(){
 
    	if ($this->bj == null) {

    		if ($this->satuan == 'M3') {
    			$this->hasil = $this->get_m3();
    			return $this->hasil;
    		} elseif ($this->satuan == 'M2') {
    			$this->hasil = $this->get_m2();
    			return $this->hasil;
    		}
    		else {
    			return 0;
    		}
    		
    	} else {
    		
    		if ($this->satuan_bj == 'ton/m3') {
    			$this->hasil = $this->get_bj_m3();
    			return $this->hasil;
    		} elseif ($this->satuan_bj == 'l/m2') {
    			$this->hasil = $this->get_bj_m2();
    			return $this->hasil;
    		} elseif ($this->satuan_bj == 'kg/m3') {
    			$this->hasil = $this->get_bj_m3();
    			return $this->hasil;
    		}
    		else {
    			return 0;
    		}
    	}
    	
    }

    public function get_m3(){

    	$this->hasil = $this->panjang * $this->lebar * $this->tebal * $this->hsp;
    	return $this->hasil;
    }

    public function get_m2(){

    	$this->hasil = $this->panjang * $this->lebar * $this->hsp;
    	return $this->hasil;
    }

    public function get_bj_m3(){

    	$this->hasil = $this->panjang * $this->lebar * $this->tebal * $this->bj * $this->hsp;
    	return $this->hasil;
    }

    public function get_bj_m2(){

    	$this->hasil = $this->panjang * $this->lebar * $this->bj * $this->hsp;
    	return $this->hasil;
    }
    

   }
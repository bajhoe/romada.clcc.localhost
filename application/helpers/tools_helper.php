<?php defined('BASEPATH') OR exit('No direct script access allowed');

function dtc($data) {
    $output = $data;
    if ( is_array($output) ){
        //$output = implode( ',', $output);
    	$output = json_encode($output);
    }
    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}

function unit_sup($data) {
	if ($data != NULL){
		if ($data == 'm2' || $data == 'M2') {
			return "M<sup>2</sup>";
		} else if ($data == 'm3' || $data == 'M3') {
			return "M<sup>3</sup>";
		} else if ($data == 'l/m2' || $data == 'l/M2') {
			return "L/M<sup>2</sup>";
		} else if ($data == 'ton/m3' || $data == 'ton/M3') {
			return "Ton/M<sup>3</sup>";
		} 

		else {
			return $data;
		}
	}	
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

//mysql to leaflet
function latlon($datas) {
   
	$latlon_final = Array();

	foreach ($datas as $data){
		array_push($latlon_final, [$data['lat'], $data['lon']]);
	}

	$latlon_final = json_encode($latlon_final);

	return $latlon_final;
}


function convertsegment1($datas) {
   
	$latlon_final = Array();

	foreach ($datas as $data){
		array_push($latlon_final, [$data['lat_a'], $data['lon_a'],$data['lat_b'], $data['lon_b'], number_format($data['jarak'],2), $data['no_segment']]);
	}

	$latlon_final = json_encode($latlon_final);

	return $latlon_final;
}

function convertsegment2($datas) {
   
	$latlon_final = Array();

	foreach ($datas as $data){
		array_push($latlon_final, [$data['lat_a'], $data['lon_a']]);
	}

	$latlon_final = json_encode($latlon_final);

	return $latlon_final;
}


//comma to period

function comper($data){

	//return str_replace(',', '.', $data);
	return $data;

}